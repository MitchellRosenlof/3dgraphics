Author: Mitch Rosenlof
Instructor: Professor Griffith
Class: CST-310
Project: Sierpinski Gasket

Instructions on running program:
-Simply run the .HTML files in any of these 5 browsers:
Chrome, Firefox, Safari, Edge, Opera

Software Requirements:
-One of the 5 web browsers listed above.
-JavaScript compile (JDK or JRE)