function triangle(a, b, c)
{
    //store the points of the triangle
    points.push(a);
    points.push(b);
    points.push(c);
}

function divideTriangle(a, b, c, count)
{
    //end of recursive generation
    if (count == 0)
    {
        triangle(a, b, c);
    }else{
        //calculate bisecting vertices of each new triangle
        var ab = mix(a, b, 0.5);
        var ac = mix(a, c, 0.5);
        var bc = mix(b, c, 0.5);

        --count;

        //recurse using these newfound vertices
        divideTriangle(a, ab, ac, count);
        divideTriangle(c, ac, bc, count);
        divideTriangle(b, bc, ab, count);
    }

}
var numSubdivisions = 5;
var gl;
var points = [];
window.onload = function myFunction() {
    //store canvas object from HTML file
    var canvas = document.getElementById("gl-canvas");

    //create a graphics library object
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("WebGL is not available."); }

    //vertices of original triangle
    var verts = [
        vec2(-1, -1),
        vec2(0, 1),
        vec2(1, -1)
    ];

    divideTriangle(verts[0], verts[1], verts[2], numSubdivisions);

    //configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    //load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    //load the data into the GPU
    var bufferID = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferID);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW);

    //Associate our shader variables with our data buffer
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    //render the graphics to the canvas using WebGL
    render();
}
function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, points.length);
}