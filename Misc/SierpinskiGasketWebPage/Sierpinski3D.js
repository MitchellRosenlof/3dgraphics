var numSubdivisions = 3;

var gl;
var canvas;

var points = [];
var colors = [];

window.onload = function init() {
    //store canvas object from HTML file
    canvas = document.getElementById("gl-canvas");

    //create a graphics library object
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("WebGL is not available."); }

    //vertices of original triangle
    var verts = [
        vec3( 0.0, -0.2, -1.0 ),
        vec3( 0.0,  1.0,  0.0 ),
        vec3(-1.0, -0.5,  0.0 ),
        vec3( 1.0, -0.5,  0.0 )
    ];

    //calculate all the points of the gasket recursively
    divideTetra(verts[0], verts[1], verts[2], verts[3], numSubdivisions);

    //  Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // enable hidden-surface removal
    gl.enable(gl.DEPTH_TEST);

    // Load shaders and initialize buffers
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    //
    //create a buffer object for color data and send to GPU
    //
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    //
    //create a buffer object for vertex data and send to GPU
    //
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    //render all graphics to the screen
    render();
};
function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, points.length);
}

function triangle(a, b, c, color)
{
    //colors for each tetrahedron side
    var baseColors = [
        vec3(1.0, 0.0, 0.0),
        vec3(0.0, 1.0, 0.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 0.0)
    ];

    //store the color data and color data
    colors.push(baseColors[color]);
    points.push(a);
    colors.push(baseColors[color]);
    points.push(b);
    colors.push(baseColors[color]);
    points.push(c);
}

function tetra(a, b, c, d)
{
    //data for each tetra is composed of 4 triangles
    triangle(a, c, b, 0);
    triangle(a, c, d, 1);
    triangle(a, b, d, 2);
    triangle(b, c, d, 3);
}

function divideTetra(a, b, c, d, count)
{
    //end of recursive generation
    if (count == 0)
    {
        tetra(a, b, c, d);
    }else{
        //find the bisecting vertices of each new tetra
        var ab = mix(a, b, 0.5);
        var ac = mix(a, c, 0.5);
        var ad = mix(a, d, 0.5);
        var bc = mix(b, c, 0.5);
        var bd = mix(b, d, 0.5);
        var cd = mix(c, d, 0.5);

        --count;

        //recurse using these newfound vertices
        divideTetra(a, ab, ac, ad, count);
        divideTetra(ab, b, bc, bd, count);
        divideTetra(ac, bc, c, cd, count);
        divideTetra(ad, bd, cd, d, count);
    }
}