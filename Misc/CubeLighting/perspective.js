var gl;
var canvas;
var program;
var points = [];
var colors = [];
var normals = [];
var numVertices = 0;

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;

var axis = 0;
var theta = [ 0, 0, 0 ];

var thetaLoc;

var near = 0.3;
var far = 3.0;

var fovy = 45.0;
var aspect;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;

var lightAmbient = vec4(0.2, 0.2, 0.2, 1.0);
var lightDiffuse = vec4(1.0, 1.0, 1.0, 1.0);
var lightSpecular = vec4(1.0, 1.0, 1.0, 1.0);
var materialAmbient = vec4(1.0, 0.0, 1.0, 1.0);
var materialDiffuse = vec4(1.0, 0.0, 0.0, 1.0);
var materialSpecular = vec4(1.0, 0.0, 0.0, 1.0);
var materialShininess = 100.0;

var eyePt;
var atPt;
var upVec;

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [0,0,0];

var faceColors = [
    [ 0.0, 0.0, 0.0, 1.0 ],  // black
    [ 1.0, 0.0, 0.0, 1.0 ],  // red
    [ 1.0, 1.0, 0.0, 1.0 ],  // yellow
    [ 0.0, 1.0, 0.0, 1.0 ],  // green
    [ 0.0, 0.0, 1.0, 1.0 ],  // blue
    [ 1.0, 0.0, 1.0, 1.0 ],  // magenta
    [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
    [ 1.0, 1.0, 1.0, 1.0 ]   // white
];

window.onload = function init()
{
    canvas = document.getElementById("gl-canvas");

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("not available."); }

    buildCube();

    gl.viewport(0, 0, canvas.height, canvas.width);
    gl.clearColor(1.0, 1.0, 1.0, 1);
    gl.enable(gl.DEPTH_TEST);

    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);
	
	lightx = document.getElementById("lightsliderx");
	lighty = document.getElementById("lightslidery");
	lightz = document.getElementById("lightsliderz");
	eyex = document.getElementById("eyesliderx");
	eyey = document.getElementById("eyeslidery");
	eyez = document.getElementById("eyesliderz");
	atx = document.getElementById ("atsliderx" );
	aty = document.getElementById ("atslidery" );
	atz = document.getElementById ("atsliderz" );
	upx = document.getElementById ("upsliderx" );
	upy = document.getElementById ("upslidery" );
	upz = document.getElementById ("upsliderz" );
	document.getElementById( "xButton" ).onclick = function () {
        axis = xAxis;
    };
    document.getElementById( "yButton" ).onclick = function () {
        axis = yAxis;
    };
    document.getElementById( "zButton" ).onclick = function () {
        axis = zAxis;
    };

	loadVertsAndNormals();
    loadColors();
	setVSVectors();
    render();
}

function loadColors()
{
    // Load the colors for the triangles and enable the attribute vColor
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
}

function loadVertsAndNormals()
{
	var nBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, nBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW);
	
	var vNormal = gl.getAttribLocation(program, "vs_Normal");
	gl.vertexAttribPointer(vNormal, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(vNormal);
	
	// Load the vertices for the triangles and enable the attribute vPosition
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
}

function buildCube()
{
	buildFace(1, 0, 3, 2);
	buildFace(2, 3, 7, 6);
	buildFace(3, 0, 4, 7);
	buildFace(6, 5, 1, 2);
	buildFace(4, 5, 6, 7);
	buildFace(5, 4, 0, 1);
}

function buildFace(a, b, c, d)
{
	var verts = [
		vec4( -0.5, -0.5,  0.5, 1.0),
		vec4( -0.5,  0.5,  0.5, 1.0),
		vec4(  0.5,  0.5,  0.5, 1.0),
		vec4(  0.5, -0.5,  0.5, 1.0),
		vec4( -0.5, -0.5, -0.5, 1.0),
		vec4( -0.5,  0.5, -0.5, 1.0),
		vec4(  0.5,  0.5, -0.5, 1.0),
		vec4(  0.5, -0.5, -0.5, 1.0)		
	];
	
	var v1 = subtract(verts[b], verts[a]);
	var v2 = subtract(verts[c], verts[b]);
	var normal = vec3(cross(v1, v2));

	let indices = [a, b, c, a, c, d];
	for (var i = 0; i < indices.length; ++i)
	{
		points.push(verts[indices[i]]);
		normals.push(normal);
		numVertices += 1;
		colors.push(faceColors[4]);
	}
}

function setVSVectors()
{
	var lightPosition = vec4(lightsliderx.value, lightslidery.value, lightsliderz.value, 0.0);
   	var ambientProduct  = mult(lightAmbient, materialAmbient);
	var diffuseProduct  = mult(lightDiffuse, materialDiffuse);
	var specularProduct = mult(lightSpecular, materialSpecular);
	
	gl.uniform4fv(gl.getUniformLocation(program, "vs_AmbientProduct"),
	flatten(ambientProduct));
	gl.uniform4fv(gl.getUniformLocation(program, "vs_DiffuseProduct"),
	flatten(diffuseProduct));
	gl.uniform4fv(gl.getUniformLocation(program, "vs_SpecularProduct"),
	flatten(specularProduct));
	gl.uniform4fv(gl.getUniformLocation(program, "vs_LightPosition"),
	flatten(lightPosition));
	gl.uniform1f(gl.getUniformLocation(program, "vs_Shininess"),
	materialShininess);
}

function setViewProjection()
{
	VMatrixLoc = gl.getUniformLocation(program, "vs_ViewMatrix");
	PMatrixLoc = gl.getUniformLocation(program, "vs_ProjMatrix");	
	
	if (!VMatrixLoc || !PMatrixLoc){ console.log("failed"); }
	
	eyePt = vec3(eyex.value, eyey.value, eyez.value);
	atPt = vec3(atx.value, aty.value, atz.value);
	upVec = vec3(upx.value, upy.value, upz.value);
	
	vs_ViewMatrix = lookAt(eyePt, atPt, upVec);
	
	vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0]));
	vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0]));
	vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1]));
	
	fovy = 50;
	aspect = canvas.width/canvas.height;
	near = 1;
	far = 100;
	
	vs_ProjMatrix = perspective(fovy, aspect, near, far);
	
	gl.uniformMatrix4fv(VMatrixLoc, false, flatten(vs_ViewMatrix));
	gl.uniformMatrix4fv(PMatrixLoc, false, flatten(vs_ProjMatrix));
}

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.clearColor(0.0, 0.0, 0.0, 0.1);
	
	setViewProjection();
	setVSVectors();
    theta[axis] += 1.0;

    gl.drawArrays( gl.TRIANGLES, 0, numVertices );

    requestAnimFrame( render );
}