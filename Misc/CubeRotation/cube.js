var gl;

window.onload = function init()
{
    //initialize canvas
    var canvas = document.getElementById('gl-canvas');

    //initialize WebGL
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("WebGL not available."); }

    //set up WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);

    //set up program in webgl
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    var vBuffer = gl.createBuffer();





    render();
}

function render()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    theta[axis] += 2.0;
}