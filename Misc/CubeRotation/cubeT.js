"use strict";

var canvas;
var gl;

var NumVertices  = 36;  // (2 triangles per face or 6 vertices per face) for 6 faces

var points = [];
var colors = [];

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;

var axis = 0;
var theta = [ 0, 0, 0 ];

var thetaLoc;

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    buildCube();

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

	// Activates depth comparisons and updates to the depth buffer
	
    gl.enable(gl.DEPTH_TEST);     

    //
    //  Compile, Link shaders into program
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

	loadVertices(program);
	
	loadColors(program);

    // get the location of the angle

    thetaLoc = gl.getUniformLocation(program, "theta");

    //event listeners for buttons

    document.getElementById( "xButton" ).onclick = function () {
        axis = xAxis;
    };
    document.getElementById( "yButton" ).onclick = function () {
        axis = yAxis;
    };
    document.getElementById( "zButton" ).onclick = function () {
        axis = zAxis;
    };

	console.log(points);
    render();
}

function loadVertices(program) {

    // Load the vertices for the triangles and enable the attribute vPosition

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );
    
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

}


function loadColors(program) {

    // Load the colors for the triangles and enable the attribute vColor
   
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
}


function buildCube() {

	// Build all 6 faces of the cube

    //buildFace( 1, 0, 3, 2 );    // Cube Face 1
	
	buildFace( 5, 4, 0, 1 );    // Cube Face 6
    buildFace( 8, 9, 3, 2 );    // opened side
    buildFace( 6, 5, 1, 2 );    // Cube Face 4
    buildFace( 4, 5, 6, 7 );    // Cube Face 5
	buildFace( 2, 3, 7, 6 );    // Cube Face 2
    buildFace( 3, 0, 4, 7 );    // Cube Face 3

}


function buildFace(a, b, c, d) {

    var vertices = [
        vec4( -0.25, -0.25,  0.25, 1.0 ),	// vertex 0
        vec4( -0.25,  0.25,  0.25, 1.0 ),   // vertex 1
        vec4(  0.25,  0.25,  0.25, 1.0 ),   // vertex 2
        vec4(  0.25, -0.25,  0.25, 1.0 ),   // vertex 3
        vec4( -0.25, -0.25, -0.25, 1.0 ),   // vertex 4
        vec4( -0.25,  0.25, -0.25, 1.0 ),   // vertex 5
        vec4(  0.25,  0.25, -0.25, 1.0 ),   // vertex 6
        vec4(  0.25, -0.25, -0.25, 1.0 ),   // vertex 7
        vec4( -0.16,  0.25,  0.50, 1.0 ),   //opened vertices
        vec4( -0.16, -0.25,  0.50, 1.0 )
    ];

    var faceColors = [
        [ 0.0, 0.0, 0.0, 1.0 ],  // black
        [ 1.0, 0.0, 0.0, 1.0 ],  // red
        [ 1.0, 1.0, 0.0, 1.0 ],  // yellow
        [ 0.0, 1.0, 0.0, 1.0 ],  // green
        [ 0.0, 0.0, 1.0, 1.0 ],  // blue
        [ 1.0, 0.0, 1.0, 1.0 ],  // magenta
        [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
        [ 1.0, 1.0, 1.0, 1.0 ]   // white
    ];

    // WebGL renders triangles.  We need to split the cube face into
    // two triangles.  The trick below creates triangle a,b,c then
    // a,c,d from face a,b,c,d.  It then colors the face the color
    // of the first passed in vertice "a".  So don't call this function with
    // the same starting vertice (otherwise you will have faces with the
    // same color)


    var indices = [ a, b, c, a, c, d ];

    for ( var i = 0; i < indices.length; ++i ) {
        
        // Add to array each triangle
        
        points.push( vertices[indices[i]] );

        // Color the two triangles the same color
        if (a >= 8)
        {
            colors.push(faceColors[1]);
        }else{
            colors.push(faceColors[a]);
        }
    }
}


function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    theta[axis] += 2.0;
    gl.uniform3fv(thetaLoc, theta);

    gl.drawArrays( gl.TRIANGLES, 0, NumVertices );

    requestAnimFrame( render );
}