class Cube{
    constructor(x, y, z){
		this.instanceMatrix = mat4();
		this.loc = [x, y, z];
		var r = Math.floor(Math.random() * 3);
		
		if (r == 1){ this.axis = [1, 0, 0]; }
		else if (r == 2){ this.axis = [0, 1, 0]; }
		else{ this.axis = [0, 0, 1]; }
		
		var vertices = [

			vec4( -0.5, -0.5,  0.5, 1.0 ),
			vec4( -0.5,  0.5,  0.5, 1.0 ),
			vec4( 0.5,  0.5,  0.5, 1.0 ),
			vec4( 0.5, -0.5,  0.5, 1.0 ),
			vec4( -0.5, -0.5, -0.5, 1.0 ),
			vec4( -0.5,  0.5, -0.5, 1.0 ),
			vec4( 0.5,  0.5, -0.5, 1.0 ),
			vec4( 0.5, -0.5, -0.5, 1.0 )
		];
		
		var face1 = [0, 1, 2, 0, 2, 3];
		var face2 = [3, 2, 6, 3, 6, 7];
		var face3 = [7, 6, 5, 7, 5, 4];
		var face4 = [4, 5, 1, 4, 1, 0];
		var face5 = [1, 5, 6, 1, 6, 2];
		var face6 = [4, 0, 3, 4, 3, 7];
		
		var faceIndices = [face1, face2, face3, face4, face5, face6];
		
		for (var i = 0; i < faceIndices.length; i++)
		{
			for (var j = 0; j < faceIndices[i].length; j++)
			{
				points_array.push(vec4(vertices[ faceIndices[i][j] ]) );
				colors_array.push(faceColors[i+1]);
		    }
		}
	}
	
	rotateCube(t){
		this.instanceMatrix = mult(vs_ViewMatrix, scalem(0.1, 0.5, 0.5));
		this.instanceMatrix = mult(vs_ViewMatrix, translate(this.loc));
		this.instanceMatrix = mult(this.instanceMatrix, rotate(t, this.axis));
		gl.uniformMatrix4fv(VMatrixLoc, false, flatten(this.instanceMatrix));
		gl.drawArrays(gl.TRIANGLES, 0, 36);
	}
}