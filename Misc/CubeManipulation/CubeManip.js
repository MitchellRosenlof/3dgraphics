var canvas;
var gl;
var program;

var points_array = [];
var colors_array = [];
var cubes = [];

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;

var axis = 0;
var theta = 2;
//var theta = [ 0, 0, 0 ];

var thetaLoc;

var near = 0.3;
var far = 3.0;
var fovy = 45.0;
var aspect;

var VMatrixLoc, PMatrixLoc;

var vs_ViewMatrix;
var vs_ProjMatrix;

var faceColors = [
    [ 0.0, 0.0, 0.0, 1.0 ],  // black
    [ 1.0, 0.0, 0.0, 1.0 ],  // red
    [ 1.0, 1.0, 0.0, 1.0 ],  // yellow
    [ 0.0, 1.0, 0.0, 1.0 ],  // green
    [ 0.0, 0.0, 1.0, 1.0 ],  // blue
    [ 1.0, 0.0, 1.0, 1.0 ],  // magenta
    [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
    [ 1.0, 1.0, 1.0, 1.0 ]   // white
];

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

	for (var i = 0; i < 10; i++){
		var c = new Cube(Math.random() * (3 - (-3)) + -3, 
		                 Math.random() * (3 - (-3)) + -3,
						 Math.random() * (3 - (-3)) + -3);
		cubes.push(c);
	}
		

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 0.1, 0.1, 0.2);

	// Activates depth comparisons and updates to the depth buffer
	
    gl.enable(gl.DEPTH_TEST);     

    //
    //  Compile, Link shaders into program
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

	load_buffers();

    // get the location of the angle

    thetaLoc = gl.getUniformLocation(program, "theta");

    //event listeners for buttons

    document.getElementById( "xButton" ).onclick = function () {
        axis = xAxis;
    };
    document.getElementById( "yButton" ).onclick = function () {
        axis = yAxis;
    };
    document.getElementById( "zButton" ).onclick = function () {
        axis = zAxis;
    };
	
	eyex = document.getElementById("eyesliderx");
	eyey = document.getElementById("eyeslidery");
	eyez = document.getElementById("eyesliderz");

    render();
}

function setViewProjection()
{	
	eyePt = vec3(0, 0, 5);//eyex.value, eyey.value, eyez.value);
	atPt = vec3(0, 0, 0);
	upVec = vec3(0, 1, 0);
	
	vs_ViewMatrix = lookAt(eyePt, atPt, upVec);
	
	// vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0]));
	// vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0]));
	// vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1]));

	
	fovy = 50;
	aspect = canvas.width/canvas.height;
	near = 1;
	far = 100;
	
	vs_ProjMatrix = perspective(fovy, aspect, near, far);
	
	gl.uniformMatrix4fv(VMatrixLoc, false, flatten(vs_ViewMatrix));
	gl.uniformMatrix4fv(PMatrixLoc, false, flatten(vs_ProjMatrix));
	
	for (var i = 0; i < cubes.length; i++){
		//cubes[i].translateCube(cubes[i].loc);
		cubes[i].rotateCube(theta);
	}
}

function load_buffers()
{
	//vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points_array), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	// colors
	var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors_array), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
	//camera
	VMatrixLoc = gl.getUniformLocation(program, "vs_ViewMatrix");
	PMatrixLoc = gl.getUniformLocation(program, "vs_ProjMatrix");
	if (!VMatrixLoc || !PMatrixLoc){ console.log("failed"); }
}

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	setViewProjection();
    theta += 2.0;
	theta = theta % 360;

    requestAnimFrame( render );
}