var canvas;
var gl;
var program;

var pointsArray = [];
var colorsArray = [];
var indexBuffer = [];
var iBuffer;
var terrIndices = 0;

var xTerr = 10;
var zTerr = 10;

var faceColors = [
	[ 0.1, 0.7, 0.1, 1.0 ],  // grassy green
	[ 0.1, 0.6, 0.1, 1.0 ],
	[ 0.1, 0.5, 0.1, 1.0 ],
	[ 0.1, 0.4, 0.1, 1.0 ],
	[ 0.1, 0.3, 0.1, 1.0 ],
	[ 0.1, 0.2, 0.1, 1.0 ]
];

var near = -10;
var far = 10;
var radius = 1.0;
var theta  = 1.0;
var phi    = 0.0;
var dr = 5.0 * Math.PI/180.0;

var left = -2.0;
var right = 2.0;
var ytop = 2.0;
var bottom = -2.0;

var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;
var eye;

var eye = vec3(1.0, 1.0, 1.0);
const at = vec3(0.0, 0.0, 0.0);
const up = vec3(0.0, 1.0, 0.0);

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
	
	generateTerrain();
	plotTerrain();
	//genSphereVertices(1, 10, 10, 0,0,0);
	//plotSphere(10, 10);

    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    loadBuffers();
	
	document.getElementById("Button1").onclick = function(){theta += dr;};
    document.getElementById("Button2").onclick = function(){theta -= dr;};
    document.getElementById("Button3").onclick = function(){phi += dr;};
    document.getElementById("Button4").onclick = function(){phi -= dr;};

    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );

	console.log(pointsArray);
	console.log(indexBuffer);
    render();
}

function loadBuffers()
{
	//vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	//colors
	var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
	//indices of terrain mesh
	iBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexBuffer), gl.STATIC_DRAW);
	iBuffer.itemSize = 1;

	//camara
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
}

function generateTerrain()
{
	var heights = [];
	//grid x length centered at origin (if 10 move from -5 to 5)
    for (var i = Math.floor(-xTerr/2); i < (Math.floor(xTerr/2))+1; i++)
	{//grid z length centered at origin
	    for (var j = Math.floor(-zTerr/2); j < (Math.floor(zTerr/2))+1; j++)
		{
			//scale plane to view size coords
		    var x = i/xTerr*3;
			var z = j/zTerr*3;
			
			var y = Math.sin(z) * Math.sin(x);
			
			pointsArray.push(vec4(x, y, z, 1.0));
			colorsArray.push(faceColors[Math.abs((i)%5)]);
		}
	}
}

function plotTerrain()
{
    for (var i = 0; i < xTerr; ++i)
	{
	    for (var j = 0; j < zTerr; ++j)
		{
		    var vA = i * (xTerr+1) + j;
			var vB = vA + (zTerr+1);
			var vC = vA + 1;
			var vD = vB + 1;
			
			indexBuffer.push(vA);
			indexBuffer.push(vB);
			indexBuffer.push(vD);
			indexBuffer.push(vA);
			indexBuffer.push(vC);
			indexBuffer.push(vD);
			terrIndices+=6;
		}
	}
}

var render = function() {
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
	
	eye = vec3(radius*Math.sin(theta)*Math.cos(phi),
               radius*Math.sin(theta)*Math.sin(phi),
			   radius*Math.cos(theta));
    modelViewMatrix = lookAt(eye, at , up);
    projectionMatrix = ortho(left, right,
	                         bottom, ytop,
  							 near, far);

    gl.uniformMatrix4fv( modelViewMatrixLoc, false, flatten(modelViewMatrix) );
    gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );

    // render columns of data then rows
	for (var i = 0; i < xTerr; i++){
        gl.drawElements(gl.TRIANGLES, terrIndices/xTerr, gl.UNSIGNED_SHORT, i*2*terrIndices/xTerr);
	}

    requestAnimFrame(render);
}