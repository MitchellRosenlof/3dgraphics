"use strict";

var nRows = 50;
var nColumns = 50;

var pointsArray = [];
var points = [];
var colorsArray = [];
var indexBuffer = [];
var iBuffer;
var numVertices = 0;

var faceColors = [
    [ 0.0, 0.0, 0.0, 1.0 ],  // black
    [ 1.0, 0.0, 0.0, 1.0 ],  // red
    [ 1.0, 1.0, 0.0, 1.0 ],  // yellow
    [ 0.0, 1.0, 0.0, 1.0 ],  // green
    [ 0.0, 0.0, 1.0, 1.0 ],  // blue
    [ 1.0, 0.0, 1.0, 1.0 ],  // magenta
    [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
    [ 1.0, 1.0, 1.0, 1.0 ],  // white
	[ 1.0, 0.7, 0.0, 1.0 ],  // orange
	[ 0.1, 0.7, 0.1, 1.0 ],  // grassy green
	[ 0.4, 0.4, 0.4, 1.0 ],  //dirt
	[ 0.1, 0.5, 0.1, 1.0 ]   //leaves
];

var canvas;
var gl;
var program;

var near = -10;
var far = 10;
var radius = 1.0;
var theta  = 0.0;
var phi    = 0.0;
var dr = 5.0 * Math.PI/180.0;

var left = -2.0;
var right = 2.0;
var ytop = 2.0;
var bottom = -2.0;

var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;
var eye;

const at = vec3(0.0, 0.0, 0.0);
const up = vec3(0.0, 1.0, 0.0);

window.onload = function init() {

    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
	
	genSphereVertices(1, 10, 10, 0, 0, 0);
	plotSphere(10, 10);
	gl.cullFace(gl.BACK);

    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    // vertex array of data for nRows and nColumns of line strips

    //
    //  Load shaders and initialize attribute buffers
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    shaderAlign();

    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );

// buttons for moving viewer and changing size

    document.getElementById("Button1").onclick = function(){near  *= 1.1; far *= 1.1;};
    document.getElementById("Button2").onclick = function(){near  *= 0.9; far *= 0.9;};
    document.getElementById("Button3").onclick = function(){radius *= 2.0;};
    document.getElementById("Button4").onclick = function(){radius *= 0.5;};
    document.getElementById("Button5").onclick = function(){theta += dr;};
    document.getElementById("Button6").onclick = function(){theta -= dr;};
    document.getElementById("Button7").onclick = function(){phi += dr;};
    document.getElementById("Button8").onclick = function(){phi -= dr;};
    document.getElementById("Button9").onclick = function(){left  *= 0.9; right *= 0.9;};
    document.getElementById("Button10").onclick = function(){left *= 1.1; right *= 1.1;};
    document.getElementById("Button11").onclick = function(){ytop  *= 0.9; bottom *= 0.9;};
    document.getElementById("Button12").onclick = function(){ytop *= 1.1; bottom *= 1.1;};
	document.getElementById("Button13").onclick = function(){ 
	    numSubDivs++;
	    buildSphere();
		shaderAlign();
		render();
	};

	console.log(indexBuffer);
    render();
}

function shaderAlign()
{
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW );
	
	iBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexBuffer), gl.STATIC_DRAW);
	iBuffer.itemSize = 1;
	iBuffer.numItems = indexBuffer.length;

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
}

function genSphereVertices(radius, latBands, lonBands, X, Y, Z)
{
	var normRadius = 1;
	
    for (var lat = 0; lat < latBands + 1; ++lat)
	{
		var theta = lat * Math.PI/latBands; //0 to pi
		var z = normRadius * Math.cos(theta);
		
		for (var lon = 0; lon < lonBands + 1; ++lon)
		{
		    var phi = 2 * lon * Math.PI/lonBands;//0 to 2pi
			
			var u = Math.sqrt((normRadius * normRadius) - (z * z));
			
			console.log(u);
			var x = u * Math.cos(phi);
			var y = u * Math.sin(phi);
			
			pointsArray.push(vec4((X+x)*radius, (Y+y)*radius, (Z+z)*radius, 1.0));
			colorsArray.push(faceColors[4]);
		}
	}
}

function plotSphere(latBands, lonBands)
{
    for (var lat = 0; lat < latBands; ++lat)
	{
	    for (var lon = 0; lon < lonBands; ++lon)
		{
		    var vA = lat * (lonBands + 1) + lon;
			var vB = vA + (latBands + 1);
			var vC = vA + 1;
			var vD = vB + 1;
			
			indexBuffer.push(vA);
			indexBuffer.push(vB);
			indexBuffer.push(vC);
			indexBuffer.push(vB);
			indexBuffer.push(vD);
			indexBuffer.push(vC);
		}
	}
}


var render = function() {
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    eye = vec3(radius*Math.sin(theta)*Math.cos(phi),
               radius*Math.sin(theta)*Math.sin(phi),
			   radius*Math.cos(theta));

    modelViewMatrix = lookAt(eye, at , up);
    projectionMatrix = ortho(left, right,
	                         bottom, ytop,
  							 near, far);

    gl.uniformMatrix4fv( modelViewMatrixLoc, false, flatten(modelViewMatrix) );
    gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );

    // render columns of data then rows
    gl.drawElements(gl.LINE_STRIP, indexBuffer.length, gl.UNSIGNED_SHORT, 0);

    requestAnimFrame(render);
}
