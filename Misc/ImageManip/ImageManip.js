var img = new Image();
img.crossOrigin = "anonymous";
img.src = "https://c1.staticflickr.com/1/789/39206372450_45d483b144.jpg";

var canvas, context;

function draw(img)
{
    canvas = document.getElementById('img-canvas');
	
	context = canvas.getContext('2d');
	context.drawImage(img, 0, 0, canvas.width, canvas.height);
	
	img.style.display = 'none';
}

img.onload = function(){
	draw(this);
	var imgData = context.getImageData(0, 0, canvas.width, canvas.height);
	
	document.getElementById("gs").onclick = function(){
        for (var i = 0; i < imgData.data.length; i += 4)
	    {
	        var avg = (imgData.data[i]
            		 + imgData.data[i+1]
					 + imgData.data[i+2]) / 3;
		
		    imgData.data[i]   = avg;
		    imgData.data[i+1] = avg;
		    imgData.data[i+2] = avg;
	    }
		context.putImageData(imgData, 0, 0, 0, 0, canvas.width, canvas.height);
    };
	
	document.getElementById("invert").onclick = function(){
        for (var i = 0; i < imgData.data.length; i += 4)
	    {
	        imgData.data[i] = 255 - imgData.data[i];
			imgData.data[i+1] = 255 - imgData.data[i+1];
			imgData.data[i+2] = 255 - imgData.data[i+2];
	    }
		context.putImageData(imgData, 0, 0, 0, 0, canvas.width, canvas.height);
    };
	
	document.getElementById("r").onclick = function(){
		var r = Math.floor(Math.random() * 255);
        for (var i = 0; i < imgData.data.length; i += 4)
	    {
	        imgData.data[i] = Math.abs(imgData.data[i] - r);
			imgData.data[i+1] = Math.abs(imgData.data[i+1] - r);
			imgData.data[i+2] = Math.abs(imgData.data[i+1] - r);
	    }
		context.putImageData(imgData, 0, 0, 0, 0, canvas.width, canvas.height);
    };
	
	context.putImageData(imgData, 0, 0, 0, 0, canvas.width, canvas.height);
}