var gl;
var canvas;
var program;
var points = [];
var colors = [];
var normals = [];
var texCoordsArray = [];
var numVertices;
var loadedImageCount = 0;

var fs_textureLoc;
var objOffset = 0;
var twoTriangles = 6;
var numFaces = 0;

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [ 0, 0, 0 ];


var near = 0.3;
var far = 3.0;

var fovy = 45.0;
var aspect;
var VMatrixLoc, PMatrixLoc;
var vs_ViewMatrix, vs_ProjMatrix;


var eyePt;
var atPt;
var upVec;

var imageTextures = [
    'https://c1.staticflickr.com/5/4625/26712799268_7c59b1e10e_q.jpg',
    'https://c1.staticflickr.com/5/4665/40543467892_1aef2f9307_q.jpg',
	'https://c1.staticflickr.com/5/4787/40556861052_7982f5ca6e_q.jpg',
	'https://c1.staticflickr.com/5/4756/40543466622_a0f7c22346_q.jpg',
	'https://c1.staticflickr.com/5/4716/38775586080_cc55674d74_q.jpg',
	'https://c1.staticflickr.com/5/4714/39875195484_003850f098_q.jpg'
];

var faceColors = [
    [ 0.0, 0.0, 0.0, 1.0 ],  // black
    [ 1.0, 0.0, 0.0, 1.0 ],  // red
    [ 1.0, 1.0, 0.0, 1.0 ],  // yellow
    [ 0.0, 1.0, 0.0, 1.0 ],  // green
    [ 0.0, 0.0, 1.0, 1.0 ],  // blue
    [ 1.0, 0.0, 1.0, 1.0 ],  // magenta
    [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
    [ 1.0, 1.0, 1.0, 1.0 ]   // white
];

window.onload = function init()
{
    canvas = document.getElementById("gl-canvas");

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("not available."); }

    buildCube();

    gl.viewport(0, 0, canvas.height, canvas.width);
    gl.clearColor(1.0, 1.0, 1.0, 1);
    gl.enable(gl.DEPTH_TEST);

    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);
	
	eyex = document.getElementById("eyesliderx");
	eyey = document.getElementById("eyeslidery");
	eyez = document.getElementById("eyesliderz");
	atx = document.getElementById ("atsliderx" );
	aty = document.getElementById ("atslidery" );
	atz = document.getElementById ("atsliderz" );
	upx = document.getElementById ("upsliderx" );
	upy = document.getElementById ("upslidery" );
	upz = document.getElementById ("upsliderz" );
	document.getElementById( "xButton" ).onclick = function () {
        axis = xAxis;
    };
    document.getElementById( "yButton" ).onclick = function () {
        axis = yAxis;
    };
    document.getElementById( "zButton" ).onclick = function () {
        axis = zAxis;
    };

	initializeImages();
	loadBuffers();
    render();
}

function initializeImages()
{
    loadedImageCount = 0;
	
	for (var i = 0; i < imageTextures.length; i++)
	{
	    loadImage(imageTextures[i], i);
	}
}

function loadImage(url, txtUnit)
{
    var texture = gl.createTexture();
	
	fs_textureLoc = gl.getUniformLocation(program, "fs_texture");
	
	var image = new Image();
	
	image.onload = function(){ 
	    loadTexture(image, texture, txtUnit);
	};
	
	image.crossOrigin = "anonymous";
	image.src = url;
}

function loadTexture(image, texture, txtUnit)
{
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

	gl.activeTexture(gl.TEXTURE0 + txtUnit);
	gl.bindTexture(gl.TEXTURE_2D, texture);
		
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
	
	loadedImageCount += 1;
}

function loadBuffers()
{	
	// Load the colors for the triangles and enable the attribute vColor
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
	// Load the vertices for the triangles and enable the attribute vPosition
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	var tBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, tBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(texCoordsArray), gl.STATIC_DRAW );

    var vTexCoord = gl.getAttribLocation( program, "vTexCoord" );
    gl.vertexAttribPointer( vTexCoord, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vTexCoord );
}

function buildCube()
{
	buildFace(1, 0, 3, 2);
	buildFace(2, 3, 7, 6);
	buildFace(3, 0, 4, 7);
	buildFace(6, 5, 1, 2);
	buildFace(4, 5, 6, 7);
	buildFace(5, 4, 0, 1);
}

function buildFace(a, b, c, d)
{
	var verts = [
		vec4( -0.5, -0.5,  0.5, 1.0),
		vec4( -0.5,  0.5,  0.5, 1.0),
		vec4(  0.5,  0.5,  0.5, 1.0),
		vec4(  0.5, -0.5,  0.5, 1.0),
		vec4( -0.5, -0.5, -0.5, 1.0),
		vec4( -0.5,  0.5, -0.5, 1.0),
		vec4(  0.5,  0.5, -0.5, 1.0),
		vec4(  0.5, -0.5, -0.5, 1.0)		
	];
	
	var texCoord = [
        vec2(0, 0),
	    vec2(0, 1),
	    vec2(1, 1),
	    vec2(1, 0)
    ];

	let indices = [a, b, c, a, c, d];
	let texIndices = [1, 0, 3, 1, 3, 2];
	for (var i = 0; i < indices.length; i++)
	{
		points.push(verts[indices[i]]);
		texCoordsArray.push(texCoord[texIndices[i]]);
		numVertices += 1;
		colors.push(faceColors[7]);
	}
	numFaces++;
}

function setViewProjection()
{
	VMatrixLoc = gl.getUniformLocation(program, "vs_ViewMatrix");
	PMatrixLoc = gl.getUniformLocation(program, "vs_ProjMatrix");	
	
	if (!VMatrixLoc || !PMatrixLoc){ console.log("failed"); }
	
	eyePt = vec3(eyex.value, eyey.value, eyez.value);
	atPt = vec3(atx.value, aty.value, atz.value);
	upVec = vec3(upx.value, upy.value, upz.value);
	
	vs_ViewMatrix = lookAt(eyePt, atPt, upVec);
	
	vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[xAxis], [1, 0, 0]));
	vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[yAxis], [0, 1, 0]));
	vs_ViewMatrix = mult(vs_ViewMatrix, rotate(theta[zAxis], [0, 0, 1]));
	
	fovy = 50;
	aspect = canvas.width/canvas.height;
	near = 1;
	far = 100;
	
	vs_ProjMatrix = perspective(fovy, aspect, near, far);
	
	gl.uniformMatrix4fv(VMatrixLoc, false, flatten(vs_ViewMatrix));
	gl.uniformMatrix4fv(PMatrixLoc, false, flatten(vs_ProjMatrix));
}

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.clearColor(0.0, 0.0, 0.0, 0.1);
	
	setViewProjection();
    theta[axis] += 1.0;
	
	objOffset = 0;

    if (loadedImageCount == imageTextures.length)
	{
        for (var txtUnit = 0; txtUnit < numFaces; txtUnit++)
		{
		    gl.uniform1i(fs_textureLoc, txtUnit);
			gl.drawArrays(gl.TRIANGLES, objOffset, twoTriangles);
			objOffset += twoTriangles;
		}
    }

    requestAnimFrame( render );
}