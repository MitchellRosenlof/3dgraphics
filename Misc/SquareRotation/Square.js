var gl;
var theta = 0;
var thetaLoc;
var direction = true;
var amt = .01;
var verts;
window.onload = function init()
{
    //store canvas object from HTML file
    var canvas = document.getElementById("gl-canvas");
    var menu = document.getElementById("mymenu");
    menu.addEventListener("click", function(){
        switch(menu.selectedIndex)
        {
            case 0:
                console.log("spin direction changed");
                amt = -amt;
                break;
            case 1:
                console.log("right spin increased");
                amt +=.01;
                break;
            case 2:
                console.log("left spin increased");
                amt -=.01;
                break;
        }
    });
    window.addEventListener("keydown", function(){
        switch(event.keyCode)
        {
            case 32:
                console.log("spin direction changed");
                amt = -amt;
                break;
            case 39:
                console.log("right spin increased");
                amt -= .01;
                break;
            case 37:
                console.log("left spin increased");
                amt += .01;
                break;
        }
    });

    document.getElementById("DirectionButton").onclick = function(){ direction = !direction; };

    //create a graphics library object
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("WebGL is not available."); }

    //configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    //load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);


    //load the data into the GPU
    verts = [
        vec2(0, 1),
        vec2(1, 0),
        vec2(-1, 0),
        vec2(0, -1)
    ];
    var bufferID = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferID);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(verts), gl.STATIC_DRAW);

    //Associate our shader variables with our data buffer
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    thetaLoc = gl.getUniformLocation(program, "theta");

    //render the graphics to the canvas using WebGL
    //setInterval(render, delay);
    renderDelay();
}
function renderDelay()
{
    setTimeout(function() {
        requestAnimFrame(renderDelay);
        gl.clear(gl.COLOR_BUFFER_BIT);
        theta += (direction ? amt : -amt);
        gl.uniform1f(thetaLoc, theta);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, verts.length);
    }, 1);
}
