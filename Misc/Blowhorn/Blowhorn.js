var gl;
var thetaLoc;
var points = [];
var colors = [];
var numVertices = 78;

var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = 0;
var theta = [0,0,0];

var eye = vec3(1.0, 1.0, 1.0);
var at  = vec3(0.0, 0.0, 0.0);
var up  = vec3(0.0, 1.0, 0.0);


var faceColors = [
    [ 0.0, 0.0, 0.0, 1.0 ],  // black
    [ 1.0, 0.0, 0.0, 1.0 ],  // red
    [ 1.0, 1.0, 0.0, 1.0 ],  // yellow
    [ 0.0, 1.0, 0.0, 1.0 ],  // green
    [ 0.0, 0.0, 1.0, 1.0 ],  // blue
    [ 1.0, 0.0, 1.0, 1.0 ],  // magenta
    [ 0.0, 1.0, 1.0, 1.0 ],  // cyan
    [ 1.0, 1.0, 1.0, 1.0 ]   // white
];

window.onload = function init()
{
    var canvas = document.getElementById("gl-canvas");

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl){ alert("not available."); }

    buildHorn();

    gl.viewport(0, 0, canvas.height, canvas.width);
    gl.clearColor(1, 1, 1, 1);
    gl.enable(gl.DEPTH_TEST);

    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    loadVerts(program);
    loadColors(program);

    thetaLoc = gl.getUniformLocation(program, "theta");

    document.getElementById( "xButton" ).onclick = function () {
        axis = xAxis;
    };
    document.getElementById( "yButton" ).onclick = function () {
        axis = yAxis;
    };
    document.getElementById( "zButton" ).onclick = function () {
        axis = zAxis;
    };
    render();
}

function loadVerts(program)
{
    // Load the vertices for the triangles and enable the attribute vPosition
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
}

function loadColors(program)
{

    // Load the colors for the triangles and enable the attribute vColor
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
}


function buildHorn()
{
    buildPyr();
    buildCube();
    buildHandle();
}

function buildPyr()
{
    buildPyrFace(0, 1, 2, 1);
    buildPyrFace(0, 1, 3, 2);
    buildPyrFace(0, 3, 4, 3);
    buildPyrFace(0, 2, 4, 4);
}

function buildPyrFace(a, b, c, color)
{
    let verts = [
        vec4( 0.0,  0.0,  0.0,  1.0),
        vec4(-0.5,  0.5,  0.5,  1.0),
        vec4(-0.5, -0.5,  0.5,  1.0),
        vec4( 0.5,  0.5,  0.5,  1.0),
        vec4( 0.5, -0.5,  0.5,  1.0)
    ];

    var indices = [a, b, c];
    for (let i = 0; i < indices.length; ++i)
    {
        points.push(verts[indices[i]]);
        colors.push(faceColors[color]);
    }
}

function buildCube()
{
	let verts = [
        vec4(-0.3,  0.3,  0.0, 1.0),
        vec4(-0.3, -0.3,  0.0, 1.0),
        vec4( 0.3,  0.3,  0.0, 1.0),
        vec4( 0.3, -0.3,  0.0, 1.0),
        vec4(-0.3,  0.3, -0.5, 1.0),
        vec4(-0.3, -0.3, -0.5, 1.0),
        vec4( 0.3,  0.3, -0.5, 1.0),
        vec4( 0.3, -0.3, -0.5, 1.0)
    ];
    buildRectFace(0, 1, 2, 3, verts, 1);
    buildRectFace(0, 1, 4, 5, verts, 2);
    buildRectFace(1, 3, 5, 7, verts, 3);
    buildRectFace(0, 2, 4, 6, verts, 4);
    buildRectFace(4, 5, 6, 7, verts, 5);
    buildRectFace(2, 3, 6, 7, verts, 6);
}

function buildRectFace(a, b, c, d, verts, color)
{

    let indices = [a, b, c, b, c, d];
    for (let i = 0; i < indices.length; ++i)
    {
        points.push(verts[indices[i]]);
        colors.push(faceColors[color]);
    }
}

function buildHandle()
{
	let verts = [
        vec4(-0.2, -0.3, -0.2, 1.0),
        vec4( 0.2, -0.3, -0.2, 1.0),
        vec4(-0.2, -0.3, -0.4, 1.0),
        vec4( 0.2, -0.3, -0.4, 1.0),
		vec4(-0.2, -1.0, -0.2, 1.0),
        vec4( 0.2, -1.0, -0.2, 1.0),
        vec4(-0.2, -1.0, -0.4, 1.0),
        vec4( 0.2, -1.0, -0.4, 1.0),
    ];
    buildRectFace(0, 1, 4, 5, verts, 2);
    buildRectFace(1, 3, 5, 7, verts, 3);
    buildRectFace(0, 2, 4, 6, verts, 4);
    buildRectFace(4, 5, 6, 7, verts, 5);
    buildRectFace(2, 3, 6, 7, verts, 6);
}

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    theta[axis] += 2.0;
    gl.uniform3fv(thetaLoc, theta);

    gl.drawArrays( gl.TRIANGLES, 0, numVertices );

    requestAnimFrame( render );
}
