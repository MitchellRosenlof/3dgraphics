var camera;
var scene;
var renderer;

var dazzle = false;

var grid_width = 13;
var grid_breadth = 13;

var osc_speed = .10;
var wave_divisor = 2;
var height_factor = 7;
var from_zero = 3;
var wave_source_x = (grid_width-1) / 2;
var wave_source_z = (grid_breadth-1) / 2;;

var gap_ratio = 3.2;
var angle = 1;
var shapes = [];


window.onload = function init()
{
	//render a canvas
	var canvas = document.getElementById("my_canvas")
	renderer = new THREE.WebGLRenderer( {canvas: canvas, antialias: true} );

	//renderer settings
	renderer.setClearColor(0xe0ffff);
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(canvas.width, canvas.height);
	
	//camera and scene setup
	camera = new THREE.OrthographicCamera(canvas.width/-2, canvas.width/2, canvas.height/2, canvas.height/-2, 0.1, 1000);
	camera.position.set(grid_width * gap_ratio, 100, grid_breadth * gap_ratio);
	camera.zoom = 10;
	camera.updateProjectionMatrix();
	scene = new THREE.Scene();
	
	//create box grid
	for (var x = 0; x < grid_width; x++)
	{
	    for (var z = 0; z < grid_breadth; z++)
		{
		    var geometry = new THREE.BoxGeometry( 3, 3, 3 );
	        var material = new THREE.MeshLambertMaterial( {color: 0x00ff00} );
	        var cube = new THREE.Mesh( geometry, material );
	        cube.position.set(gap_ratio * x, 0, gap_ratio * z);
			
	        scene.add(cube);
		    shapes.push(cube);
		}
	}

	//camera view direction
	var x = shapes[grid_width * (grid_width - 1)/2 + (grid_breadth - 1)/2].position.x;
    var y = shapes[grid_width * (grid_width - 1)/2 + (grid_breadth - 1)/2].position.y + 20;
	var z = shapes[grid_width * (grid_width - 1)/2 + (grid_breadth - 1)/2].position.z;
	camera.lookAt(x, y, z);
	
	//lights
	var light = new THREE.PointLight( 0xffffff, 1);
	light.position.set(-40, 80, 70);
    scene.add(light);
	
	var light = new THREE.AmbientLight(0x404040); // soft white light
    scene.add(light); 
	
	//rainbow dazzle effect
	document.getElementById("dazzle").onclick = function () {
        dazzle = !dazzle;
    };
	
	//color reset
	document.getElementById("resetcolor").onclick = function () {
		dazzle = false;
        for (var i = 0; i < shapes.length; i++)
		{
		    shapes[i].material.color.setHex(0x00ff00);
		}
    };
	
	//wave divisor incr and decr
	document.getElementById("iw").onclick = function () {
        wave_divisor += .05;
    };
	
	document.getElementById("dw").onclick = function () {
		if (wave_divisor >= 1.5){ wave_divisor -= .05; }
    };
	
	//minimum scale
	document.getElementById("dfz").onclick = function () {
	    if (from_zero >= 1){ from_zero -= .1; }
	};
	
	document.getElementById("ifz").onclick = function () {
		from_zero += .1;
    };
	
	//maximum scale
	document.getElementById("ihf").onclick = function () {
		height_factor += .5;
    };
	
	document.getElementById("dhf").onclick = function () {
		if (height_factor >= 2){ height_factor -= .5; }
    };
	
	//osc speed
	document.getElementById("iof").onclick = function () {
		osc_speed += .01;
    };
	
	document.getElementById("dof").onclick = function () {
		osc_speed -= .01;
    };
	
	//wave source point
	document.getElementById("dwsx").onclick = function () {
	    wave_source_x--;
	};
	
	document.getElementById("iwsx").onclick = function () {
		wave_source_x++;
    };
	
	document.getElementById("dwsz").onclick = function () {
	    wave_source_z--;
	};
	
	document.getElementById("iwsz").onclick = function () {
		wave_source_z++;
    };
	
	render();
}

function render()
{
	for (var i = 0; i < grid_width; i++)
	{ 
        for(var j = 0; j < grid_breadth; j++)
		{
			//calculate distance from specified point
			var d = Math.sqrt( Math.pow(wave_source_x - i, 2) + Math.pow(wave_source_z - j, 2) ) / wave_divisor;
			
			//add it to the angle offset
            var a = angle + d;
			
			//calculate the wave iteration based on the angle offset and other factors
			var wave = ( Math.sin(a) + from_zero ) * height_factor;
			
			//scale the object based on this wave
			var shape = shapes[grid_width * i + j];
			shape.scale.y = wave;
			
			//toggle dazzle
			if (dazzle){ shape.material.color.setHex( Math.floor(wave)* 25 * 600000 ); }
		}
	}
	
	angle -= osc_speed;
	
    renderer.render(scene, camera);
	requestAnimationFrame( render );
}