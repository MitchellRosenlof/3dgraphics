function buildRectFace(a, b, c, d, color)
{	
    var texCoord = [
        vec2(0, 0),
	    vec2(0, 1),
	    vec2(1, 1),
	    vec2(1, 0)
    ];

    let verts = [a, b, c, d];
	let indices = [0, 1, 2, 0, 2, 3];
	let texIndices = [1, 0, 3, 1, 3, 2];
    for (var i = 0; i < indices.length; ++i)
	{
	    pointsArray.push(verts[indices[i]]);
		texturesArray.push(texCoord[texIndices[i]]);
		colorsArray.push(faceColors[color]);
	}
	numVertices += indices.length;
	numFaces += 1;
}

function mainBuilding()
{
	var verts = [ 
		vec4(-2.0, 1.5,  0.0, 1.0), //0
		vec4(-2.0, 0.0,  0.0, 1.0), //1
		vec4(-1.5, 0.0,  0.0, 1.0), //2
		vec4(-1.5, 1.5,  0.0, 1.0), //3
		
		vec4(-1.5, 0.8,  0.0, 1.0), //4
		vec4(-1.5, 0.8, -1.0, 1.0), //5
		vec4(-1.5, 1.5, -1.0, 1.0), //6
		
		vec4(-1.5, 0.0,  0.0, 1.0), //7
		vec4(-1.5, 0.0, -0.7, 1.0), //8
		vec4(-1.5, 0.8, -0.7, 1.0), //9
		
		vec4(-2.0, 1.5, -1.5, 1.0), //10
		vec4(-1.5, 1.5, -1.0, 1.0), //11
		
		vec4(-2.4, 1.5, -0.4, 1.0), //12
		vec4(-2.4, 1.0, 0.3, 1.0),  //13
		vec4(-2.0, 1.0, 0.3, 1.0),  //14
		vec4(-2.0, 1.5, -0.4, 1.0), //15
		
		vec4(-2.4, 1.07, 0.2, 1.0), //16
		vec4(-2.4, 0.0, 0.2, 1.0),  //17
		vec4(-2.0, 0.0, 0.2, 1.0),  //18
		vec4(-2.0, 1.07, 0.2, 1.0), //19
		
		vec4(-2.0, 1.21, 0.0, 1.0), //20
		
		vec4(-2.4, 0.0, -0.4, 1.0), //21
		
		vec4(-3.0, 2.2, -0.4, 1.0), //22
		vec4(-3.0, 0.0, -0.4, 1.0), //23
		vec4(-2.2, 0.0, -0.4, 1.0), //24
		vec4(-2.2, 2.2, -0.4, 1.0), //25
		
		vec4(-3.0, 2.2, -2.4, 1.0), //26
		vec4(-3.0, 0.0, -2.4, 1.0), //27
		
		vec4(-2.2, 1.07, -0.4, 1.0),//28
		vec4(-2.2, 1.07, -1.5, 1.0),//29
		vec4(-2.2, 2.2, -1.5, 1.0), //30
		
		vec4(-2.2, 1.5, -0.4, 1.0), //31
		vec4(-2.0, 1.07, -0.4, 1.0),//32
		
		vec4(-2.0, 0.4, 0.0, 1.0),  //33
		vec4(-2.0, 0.4, -1.5, 1.0), //34
		
		vec4(-2.0 ,1.07, -1.5, 1.0),//35
		
		vec4(-1.5, 0.4, 0.0, 1.0),  //36
		vec4(-1.5, 0.4, -1.0, 1.0), //37
		
		vec4(-0.25, 0.4, -1.0, 1.0), //38
		vec4(-0.25, 1.5, -1.0, 1.0), //39
		
		vec4(-0.1, 1.5, -1.5, 1.0),//40
		
		vec4(-1.5 , 0.4, -0.25, 1.0),//41
		vec4(-1.25, 0.4, -0.25, 1.0),//42
		vec4(-1.25, 0.4, -0.6, 1.0),//43
		
		vec4(-0.25, 0.4, -0.6, 1.0),//44
		
		vec4(-1.25, 0.6, -0.6, 1.0),//45
		vec4(-1.25, 0.0, -0.6, 1.0),//46
		vec4(-0.25, 0.0, -0.6, 1.0),//47
		vec4(-0.25, 0.6, -0.6, 1.0),//48
		
		vec4(-1.25, 0.6, -0.7, 1.0), //49
        vec4(-0.25, 0.6, -0.7, 1.0), //50
		
		vec4(-1.25, 0.4, -0.7, 1.0),//51
		vec4(-0.25, 0.4, -0.7, 1.0),//52
		
		vec4(-1.5,  0.6,  -0.25, 1.0),//53
		vec4(-1.5, 0.0, -0.25, 1.0),//54
		vec4(-1.25, 0.0, -0.25, 1.0),//55
		vec4(-1.25, 0.6, -0.25, 1.0),//56
		
		vec4(-0.75, 0.0, -0.25, 1.0),//57
		vec4(-0.75, 0.15, -0.25, 1.0),//58
		
		vec4(-1.5,  0.6,  -0.15, 1.0),//59
		vec4(-1.5, 0.0, -0.15, 1.0),//60
		vec4(-1.25, 0.0, -0.15, 1.0),//61
		vec4(-1.25, 0.6, -0.15, 1.0),//62
		
		vec4(-0.75, 0.0, -0.15, 1.0),//63
		vec4(-0.75, 0.15, -0.15, 1.0),//64
		
		vec4(-2.2, 0.0, -1.5, 1.0), //65
		vec4(-0.1, 0.0, -1.5, 1.0), //66
		vec4(-0.1, 2.2, -1.5, 1.0),  //67
		
		vec4(1.65, 0.0, -2.4, 1.0), //68
		vec4(1.65, 2.2, -2.4, 1.0),  //69
		
		vec4(-0.25, 0.4, -0.15, 1.0), //70
		vec4(-0.25, 0.6, -0.15, 1.0),  //71
		
		vec4(-0.25, 0.8, -1.0, 1.0), //72
		vec4(-0.25, 0.8, -0.15, 1.0), //73
		vec4(-0.25, 1.5, -0.15, 1.0),  //74
		
		vec4(-0.1, 1.5, -0.15, 1.0),//75
		
		vec4(-0.1, 1.5, -1.0, 1.0), //76
		vec4(-0.1, 2.2, -1.0, 1.0),  //77
		
		vec4(0.75, 2.2, -1.0, 1.0), //78
		
		vec4(-0.1, 1.8, -1.0, 1.0), //79
		vec4(-0.1, 1.5, -1.0, 1.0), //80
		vec4(-0.1, 1.5, -0.5, 1.0), //81
		vec4(-0.1, 1.8, -0.5, 1.0), //82
		
		vec4(0.75, 2.1, -1.0, 1.0), //83
		
		vec4(0.75, 1.8, -0.5, 1.0), //84 
		
		vec4(0.4, 1.5, -0.5, 1.0), //85
		vec4(0.4, 1.8, -0.5, 1.0), //86
		
		vec4(0.4, 1.5, -0.15, 1.0), //87
		vec4(0.4, 1.5, -0.5, 1.0), //88		
		
		vec4(0.4, 0.8, 1.0, 1.0), //89
		vec4(1.65, 0.8, 1.0, 1.0),  //90
		
		vec4(-0.25, 0.8, -0.25, 1.0), //91
		vec4(-0.25, 0.6, -0.25, 1.0),  //92
		
		vec4(0.4, 0.8, -0.15, 1.0), //93
		
		vec4(0.4, 0.4, -0.15, 1.0), //94
		vec4(0.4, 0.6, -0.15, 1.0),  //95
		
		vec4(-0.25, 0.8, -0.25, 1.0), //96
		vec4(-0.25, 0.6, -0.25, 1.0),  //97
		
		vec4(-0.15, 0.6, -0.15, 1.0), //98
		vec4(-0.15, 0.8, -0.15, 1.0),  //99
		
		vec4(-0.25, 0.8, -0.7, 1.0), //100
		vec4(-0.25, 0.8, -0.6, 1.0), //101
		
		vec4(-0.15, 0.6, -0.6, 1.0), //102
		vec4(-0.15, 0.8, -0.6, 1.0),  //103
		
		vec4(-0.15, 0.6, -0.7, 1.0), //104
		vec4(-0.15, 0.8, -0.7, 1.0),  //105
		
		vec4(-0.15, 0.6, -0.25, 1.0), //106
		vec4(-0.15, 0.8, -0.25, 1.0),  //107
		
		vec4(-0.15, 0.4, -0.25, 1.0),//108
		vec4(-0.15, 0.4, -0.7, 1.0), //109
		
		vec4(0.15, 0.8, -0.15, 1.0), //110
		vec4(0.15, 0.6, -0.15, 1.0), //111
		
		vec4(0.15, 0.6, -0.25, 1.0), //112
		vec4(0.15, 0.8, -0.25, 1.0), //113
		
		vec4(-0.25, 0.4, -0.25, 1.0), //114
		vec4(-0.25, 0.0, -0.25, 1.0), //115
		vec4(-0.25, 0.0, -0.15, 1.0),  //116
		
		vec4(-0.15, 0.0, -0.15, 1.0), //117
		vec4(-0.15, 0.4, -0.15, 1.0),  //118
		
		vec4(0.15, 0.4, -0.15, 1.0), //119
		vec4(0.15, 0.0, -0.15, 1.0), //120
		vec4(0.4, 0.0, -0.15, 1.0),  //121
		
		vec4(0.75, 2.2, -0.5, 1.0), //122
		
		vec4(0.4, 0.0, -0.5, 1.0),  //123
		vec4(0.4, 0.0, 0.9, 1.0), //124
		vec4(0.4, 0.85, 0.9, 1.0),  //125
		
		vec4(1.65, 1.8, -0.5, 1.0), //126
		vec4(1.65, 2.2, -0.5, 1.0), //127
		
		vec4(0.8, 0.0, 0.9, 1.0), //128
		vec4(0.8, 0.85, 0.9, 1.0), //129
		
		vec4(0.8, 0.0, 0.6, 1.0), //130
		vec4(0.8, 1.05, 0.6, 1.0),  //131
		
		vec4(1.5, 0.85, 0.9, 1.0), //132
		vec4(1.5, 0.0, 0.9, 1.0), //133
		vec4(1.5, 0.0, 0.6, 1.0), //134
		vec4(1.5, 1.05, 0.6, 1.0), //135
		
		vec4(1.65, 0.0, 0.9, 1.0), //136
		vec4(1.65, 0.85, 0.9, 1.0), //137
		
		vec4(0.8, 0.4, 0.6, 1.0), //132
		vec4(0.8, 0.4, 0.9, 1.0), //133
		vec4(1.5, 0.4, 0.9, 1.0), //134
		vec4(1.5, 0.4, 0.6, 1.0) //135
	];
	
	walls(verts);
    buildingTops(verts);
	rooves(verts);
	floors(verts);
}

function walls(verts)
{
	//west wing
    //east section
	buildRectFace( verts[0],  verts[1],  verts[2],  verts[3],  colors.ffwhite );  //front face
	buildRectFace( verts[3],  verts[4],  verts[5],  verts[6],  colors.rfwhite );    //right face above entrance
	buildRectFace( verts[4],  verts[7],  verts[8],  verts[9],  colors.rfwhite );    //right face left of entrance
	buildRectFace( verts[0],  verts[33], verts[34], verts[10], colors.lfwhite );    //left face
	
	//west section of west wing
	buildRectFace( verts[16], verts[17], verts[18], verts[19], colors.ffwhite );  //front wall (below roof)
	buildRectFace( verts[19], verts[18], verts[1],  verts[20], colors.rfwhite );    //right face of roof section
	buildRectFace( verts[12], verts[21], verts[17], verts[16], colors.lfwhite );   //left face of roof section
	buildRectFace( verts[22], verts[23], verts[24], verts[25], colors.ffwhite );  //front face
	buildRectFace( verts[26], verts[27], verts[23], verts[22], colors.lfwhite );   //left face
	buildRectFace( verts[25], verts[28], verts[29], verts[30], colors.rfwhite );    //right face
	buildRectFace( verts[31], verts[28], verts[32], verts[15], colors.bfwhite ); //north roof face
	
	//middle section
	//upper deck
	buildRectFace( verts[30],  verts[65],  verts[66],  verts[67], colors.ffwhite); //upper deck south wall
	buildRectFace( verts[26],  verts[27],  verts[68],  verts[69], colors.bfwhite); //upperdeck north wall
	
	//lower deck
    buildRectFace( verts[6],   verts[37],  verts[38],  verts[39], colors.ffwhite); //lower deck wall front
	
	//east wing
	buildRectFace( verts[50],  verts[52],  verts[70],  verts[71],  colors.lfwhite);   //left face below gap
	buildRectFace( verts[39],  verts[72],  verts[73],  verts[74],  colors.lfwhite);   //left face above gap
	buildRectFace( verts[67],  verts[40],  verts[76],  verts[77],  colors.lfwhite);   //left face perp to top
	buildRectFace( verts[79],  verts[80],  verts[81],  verts[82],  colors.lfwhite);   //"
	buildRectFace( verts[77],  verts[79],  verts[83],  verts[78],  colors.ffwhite);  //south face above corner roof
	buildRectFace( verts[82],  verts[81],  verts[85],  verts[86],  colors.ffwhite);  //south face below corner roof
	buildRectFace( verts[74],  verts[73],  verts[93],  verts[87],  colors.ffwhite);  //front face left balcony above gap
	buildRectFace( verts[71],  verts[70],  verts[94],  verts[95],  colors.ffwhite);  //" below gap
    buildRectFace( verts[96],  verts[97],  verts[71],  verts[73],  colors.lfwhite);   //left side rung left balcony
	buildRectFace( verts[73],  verts[71],  verts[98],  verts[99],  colors.ffwhite);  //right "
	buildRectFace( verts[100], verts[50],  verts[48],  verts[101], colors.lfwhite);   //left "
	buildRectFace( verts[101], verts[48],  verts[102], verts[103], colors.ffwhite);  //south "
	buildRectFace( verts[103], verts[102], verts[104], verts[105], colors.rfwhite);    //east inner "
	buildRectFace( verts[96],  verts[97],  verts[106], verts[107], colors.bfwhite); //north "
	buildRectFace( verts[99],  verts[98],  verts[106], verts[107], colors.rfwhite);    //east inner "
	buildRectFace( verts[106], verts[108], verts[109], verts[104], colors.rfwhite);    //east inner wall
	buildRectFace( verts[110], verts[111], verts[95],  verts[93],  colors.ffwhite);  //front face rung
	buildRectFace( verts[110], verts[111], verts[112], verts[113], colors.lfwhite);   //west face rung
	buildRectFace( verts[99],  verts[110], verts[113], verts[107], colors.bfwhite); //ceiling face rung
	buildRectFace( verts[96],  verts[107], verts[103], verts[101], colors.bfwhite); //ceiling face west
	buildRectFace( verts[114], verts[115], verts[116], verts[70],  colors.lfwhite);   //west face pillar
	buildRectFace( verts[70],  verts[116], verts[117], verts[118], colors.ffwhite);  //south face pillar
    buildRectFace( verts[119], verts[120], verts[121], verts[94],  colors.ffwhite);  //south face left of entrance
	buildRectFace( verts[78],  verts[83],  verts[84],  verts[122], colors.lfwhite);   //west face block above roof
	buildRectFace( verts[86],  verts[123], verts[124], verts[125], colors.lfwhite);   //west face left roof wall
	buildRectFace( verts[122], verts[84],  verts[126], verts[127], colors.ffwhite);  //south face above balcony roof
	buildRectFace( verts[125], verts[124], verts[128], verts[129], colors.ffwhite);  //south face below right balcony
	buildRectFace( verts[129], verts[128], verts[130], verts[131], colors.rfwhite);    //east face inside balcony
	buildRectFace( verts[132], verts[133], verts[134], verts[135], colors.rfwhite);    //west face inside balcony
	buildRectFace( verts[132], verts[133], verts[136], verts[137], colors.ffwhite);  //south face balcony rung
	buildRectFace( verts[131], verts[130], verts[134], verts[135], colors.ffwhite);  //baclony "sliding glass door"
	buildRectFace( verts[137], verts[136], verts[68],  verts[126], colors.rfwhite);    //west face
	buildRectFace( verts[127], verts[126], verts[68],  verts[69],  colors.rfwhite);    //"
	buildRectFace( verts[97],  verts[106], verts[102], verts[48],  colors.bfwhite);//east inner "
	buildRectFace( verts[106], verts[98],  verts[111], verts[112], colors.bfwhite);//west face rung
	
	//staircase railing inner
	buildRectFace( verts[45],  verts[46],  verts[47],  verts[48], colors.ffwhite);//staircase wall inner front
    buildRectFace( verts[49],  verts[45],  verts[48],  verts[50], colors.bfwhite);//starcase wall inner top
	buildRectFace( verts[49],  verts[51],  verts[52],  verts[50], colors.bfwhite);//staircase wall inner back
	buildRectFace( verts[49],  verts[51],  verts[43],  verts[45], colors.lfwhite);//staircase wall inner west face
	
	//staircase railing outer
	buildRectFace( verts[53],  verts[54],  verts[55],  verts[56], colors.bfwhite);//staircase wall outer back face
    buildRectFace( verts[56],  verts[55],  verts[57],  verts[58], colors.bfwhite);//"
	buildRectFace( verts[59],  verts[60],  verts[61],  verts[62], colors.ffwhite);//staircase wall outer front face
	buildRectFace( verts[62],  verts[61],  verts[63],  verts[64], colors.ffwhite);//"
	buildRectFace( verts[53],  verts[59],  verts[62],  verts[56], colors.bfwhite);//staircaise wall top
	buildRectFace( verts[56],  verts[62],  verts[64],  verts[58], colors.bfwhite);//"
	buildRectFace( verts[64],  verts[63],  verts[57],  verts[58], colors.bfwhite);//"
}

function buildingTops(verts)
{
    buildRectFace( verts[10], verts[0],  verts[3],  verts[11], colors.ffwhite );//top east wing
	buildRectFace( verts[29], verts[28], verts[32], verts[35], colors.ffwhite );  //flat roof
	buildRectFace( verts[26], verts[22], verts[25], verts[30], colors.ffwhite );//top
	buildRectFace( verts[30],  verts[67],  verts[69],  verts[26], colors.ffwhite); //top
	buildRectFace( verts[6],   verts[39],  verts[40],  verts[10], colors.ffwhite); //lower deck top 
	buildRectFace( verts[39],  verts[74],  verts[75],  verts[40],  colors.ffwhite);//top east wing
	buildRectFace( verts[67],  verts[77],  verts[78],  verts[69],  colors.ffwhite);//"
	buildRectFace( verts[81],  verts[75],  verts[87],  verts[88],  colors.ffwhite);//top below corner roof
	buildRectFace( verts[78],  verts[122], verts[127], verts[69],  colors.ffwhite) //upper deck top
}

function rooves(verts)
{
    buildRectFace( verts[12], verts[13], verts[14], verts[15], colors.ffwhite );   //left roof
	buildRectFace( verts[83],  verts[79],  verts[82],  verts[83],  colors.ffwhite );  //corner roof
    buildRectFace( verts[83],  verts[84],  verts[82],  verts[83],  colors.ffwhite );  //"
	buildRectFace( verts[86],  verts[89],  verts[90],  verts[126], colors.ffwhite);   //roof extending down to east balcony
}

function floors(verts)
{
    buildRectFace( verts[34], verts[33], verts[36], verts[37], colors.ffwhite );  //inside second floor
	buildRectFace( verts[37],  verts[41],  verts[42],  verts[43], colors.ffwhite); //lower deck floor
	buildRectFace( verts[37],  verts[43],  verts[44],  verts[38], colors.ffwhite); //"
	buildRectFace( verts[138], verts[139], verts[140], verts[141], colors.ffwhite);  //balcony floor
	
	generateStaircase(verts[42], verts[43], 8);
}

function generateStaircase(a, b, count)
{
	if (count > 0)
	{
	    //drop to next stair (create base)
		var drop_a = vec4(a[0], a[1]-.05, a[2], 1.0);
		var drop_b = vec4(b[0], a[1]-.05, b[2], 1.0);
		
	    buildRectFace( a,  drop_a,  drop_b, b, colors.rfwhite);
		
	    //create stair
		var stair_a = vec4(drop_a[0]+.05, drop_a[1], drop_a[2], 1.0);
		var stair_b = vec4(drop_b[0]+.05, drop_b[1], drop_b[2], 1.0);
		
		buildRectFace( drop_a,  stair_a,  stair_b, drop_b, colors.ffwhite);
		
		//make specified amount of steps
		generateStaircase(stair_a, stair_b, count - 1);
	}
}