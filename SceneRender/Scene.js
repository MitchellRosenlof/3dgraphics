var canvas;
var program;
var gl;

var numVertices = 0;
var loadedImageCount = 0;
var numFaces = 0;
var twoTriangles = 6;
var txtUnit = 0;

var fs_textureLoc;

var pointsArray = [];
var colorsArray = [];
var texturesArray = [];
var indexBuffer = [];
var iBuffer;
var imageTextures = [
    'https://c1.staticflickr.com/5/4787/39936905524_1eafd62d0b_q.jpg',//walls
    'https://c1.staticflickr.com/5/4757/39750586055_caf5242b38_q.jpg',//tops
	'https://c1.staticflickr.com/5/4800/38836263970_a98a90e766_q.jpg',//rooves
	'https://c1.staticflickr.com/5/4658/39936955604_0436b47ed2_q.jpg'//sidewalk/ground
];

var faceColors = [
    [ 0.3, 0.0, 0.0, 1.0 ],  // browish red (rooves)
	[ 0.1, 0.7, 0.1, 1.0 ],  // grassy green
	[ 0.4, 0.4, 0.4, 1.0 ],  //dirt
	[ 0.1, 0.5, 0.1, 1.0 ],   //leaves
	[ 1.0, 1.0, 1.0, 1.0 ],   //front face white
	[ 0.8, 0.8, 0.8, 1.0 ], //right face white
	[ 0.7, 0.7, 0.7, 1.0 ], //left face white
	[ 0.5, 0.5, 0.5, 1.0 ] //back face white
];
var colors = {
	red  : 0,
	grass  : 1,
	dirt   : 2,
	leaves : 3,
	ffwhite: 4,
	rfwhite: 5,
	lfwhite: 6,
	bfwhite: 7
	
}


var near = -20;
var far = 20;

var left = -2.0;
var right = 2.0;
var ytop = 2.0;
var bottom = -2.0;

var modelViewMatrix, projectionMatrix;

var eye;
const at = vec3(0.0, 1.0, 0.0);
const up = vec3(0.0, 1.0, 0.0);

function initializeImages()
{
    loadedImageCount = 0;
	
	for (var i = 0; i < imageTextures.length; ++i)
	{
	    loadImage(program, imageTextures[i], i);
	}
}

function loadImage(program, url, txtUnit)
{
    var texture = gl.createTexture();
	
	fs_textureLoc = gl.getUniformLocation(program, "fs_texture");
	
	var image = new Image();
	
	image.onload = function(){ 
	    loadTexture(image, texture, txtUnit);
	};
	
	image.crossOrigin = "anonymous";
	image.src = url;
}

function loadTexture(image, texture, txtUnit)
{
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
	
	gl.activeTexture(gl.TEXTURE0 + txtUnit);
	gl.bindTexture(gl.TEXTURE_2D, texture);
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
	
	loadedImageCount += 1;
}

function loadBuffers()
{	
	// Load the colors for the triangles and enable the attribute vColor
	var cBuffer = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
	gl.bufferData( gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW );

	var vColor = gl.getAttribLocation( program, "vColor" );
	gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
	gl.enableVertexAttribArray( vColor );

	
	// Load the vertices for the triangles and enable the attribute vPosition
	var vBuffer = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
	gl.bufferData( gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW );

	var vPosition = gl.getAttribLocation( program, "vPosition" );
	gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
	gl.enableVertexAttribArray( vPosition );
	
	var tBuffer = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, tBuffer );
	gl.bufferData( gl.ARRAY_BUFFER, flatten(texturesArray), gl.STATIC_DRAW );

	var vTexCoord = gl.getAttribLocation( program, "vTexCoord" );
	gl.vertexAttribPointer( vTexCoord, 2, gl.FLOAT, false, 0, 0 );
	gl.enableVertexAttribArray( vTexCoord );
	
	iBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexBuffer), gl.STATIC_DRAW);
	iBuffer.itemSize = 1;
	iBuffer.numItems = indexBuffer.length;
}

window.onload = function init() {
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
	gl.clearColor(0.1, 0.6, 0.9, 1.0);//blue sky
	
	gl.enable(gl.DEPTH_TEST);

    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
	
	initializeImages();

    mainBuilding();
	ground();
	tree();
	hydrant();

	loadBuffers();

	//sliders for camera
	eyex = document.getElementById("eyesliderx");
	eyey = document.getElementById("eyeslidery");
	eyez = document.getElementById("eyesliderz");
	
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
	
	console.log(indexBuffer);
	console.log(pointsArray);
	console.log(colorsArray);
	
    render();
}


function render()
{
	//draw sky
	gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	//find camera view
	eye = vec3(eyex.value, eyey.value, eyez.value);
	modelViewMatrix = lookAt(eye, at , up);
	projectionMatrix = ortho(left, right, bottom, ytop, near, far);

	//convert to GLSL
	gl.uniformMatrix4fv( modelViewMatrixLoc, false, flatten(modelViewMatrix) );
	gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );
	
	
	objOffset = 0;

	if (loadedImageCount == imageTextures.length)
	{
		for (var i = 0; i < numFaces; ++i)
		{
			if (i < 60){//walls
				gl.uniform1i(fs_textureLoc, 0);
			}else if (i < 69){//building tops
				gl.uniform1i(fs_textureLoc, 1);
			}else if (i < 73){//slanted rooves
				gl.uniform1i(fs_textureLoc, 2);
			}else if (i < 93){//stairs
				gl.uniform1i(fs_textureLoc, 3);
			}
			gl.drawArrays(gl.TRIANGLES, objOffset, twoTriangles);
			objOffset += twoTriangles;
		}
	}
	//I have not figured out how to render these points yet
	gl.drawElements(gl.TRIANGLES, iBuffer.numItems, gl.UNSIGNED_SHORT, 0);

	//animate scene
	requestAnimFrame(render);
}