function ground()
{	
    sidewalk();
	grassAndDirt();
}

function sidewalk(verts)
{
	var verts = [
	    vec4(-0.85, 0.0, -0.6, 1.0), //0
		vec4(-0.85, 0.0, -0.15, 1.0), //1
		vec4(0.5, 0.0, -0.15, 1.0), //2
		vec4(0.5, 0.0, -0.6, 1.0), //3
		
		vec4(-1.5, 0.0, 0.25, 1.0), //4
		vec4(-1.5, 0.0, 0.5, 1.0), //5
		vec4(-0.25, 0.0, -0.15, 1.0), //6
		
		vec4(-3.0, 0.0, 0.25, 1.0), //7
		vec4(-3.0, 0.0, 0.5, 1.0), //8
		
		vec4(-0.15, 0.0, -0.15, 1.0), //9
		vec4(-0.15, 0.0, 1.3, 1.0), //10
		vec4(0.15, 0.0, 0.9, 1.0), //11
		vec4(0.15, 0.0, -0.15, 1.0), //12
		
		vec4(1.65, 0.0, 1.3, 1.0), //13
		vec4(1.65, 0.0, 0.9, 1.0),   //14
		
		vec4(0.7, 0.0, 0.5, 1.0), //15
		vec4(0.7, 0.0, 0.9, 1.0),//16
		vec4(1.5, 0.0, 0.9, 1.0),//17
		vec4(1.5, 0.0, 0.5, 1.0)//18
	];
	
	//branching left
    buildRectFace( verts[0],  verts[1],  verts[2],  verts[3],  colors.ffwhite);
	buildRectFace( verts[4],  verts[5],  verts[6],  verts[1],  colors.ffwhite);
	buildRectFace( verts[7],  verts[8],  verts[5],  verts[4],  colors.ffwhite);
	
	//branching right
	buildRectFace( verts[9],  verts[10], verts[11], verts[12], colors.ffwhite);
	buildRectFace( verts[11], verts[10], verts[13], verts[14], colors.ffwhite);
	buildRectFace( verts[15], verts[16], verts[17], verts[18], colors.ffwhite);
}

function grassAndDirt()
{
    var verts = [
	    vec4(-3.0, 0.0, -0.5, 1.0), //0
		vec4(-3.0, 0.0, 0.25, 1.0),//1
		vec4(-1.5, 0.0, 0.25, 1.0),//2
		vec4(-0.85, 0.0, -0.15, 1.0),//3
		
		vec4(-0.25, 0.0, -0.15, 1.0), //4
		vec4(-3.0, 0.0, 1.3, 1.0), //5
		vec4(-0.15, 0.0, 1.3, 1.0),  //6
		vec4(-0.15, 0.0, -0.15, 1.0), //7
		
		vec4(-3.0, 0.0, 0.5, 1.0), //8
		vec4(-1.4, 0.0, 0.5, 1.0), //9
		
		vec4(-3.0, 0.0, 2.0, 1.0), //10
		vec4(1.65, 0.0, 2.0, 1.0),  //11
		vec4(1.65, 0.0, 1.3, 1.0), //12
		
		vec4(2.5, 0.0, 2.0, 1.0), //13
		vec4(2.5, 0.0, -2.4, 1.0),  //14
		vec4(1.65, 0.0, -2.4, 1.0), //15
		
		vec4(0.15, 0.0, -0.15, 1.0), //16
		vec4(0.15, 0.0, 0.9, 1.0),//17
		vec4(0.4, 0.0, 0.9, 1.0),//18
		vec4(0.4, 0.0, -0.15, 1.0)//19
	];
	
	buildRectFace( verts[0], verts[1], verts[2], verts[3], colors.dirt);
	buildRectFace( verts[4], verts[5], verts[6], verts[7], colors.grass);
	buildRectFace( verts[8], verts[5], verts[9], verts[9], colors.grass);
	buildRectFace( verts[5], verts[10], verts[11], verts[12], colors.grass);
	buildRectFace( verts[11], verts[13], verts[14], verts[15], colors.grass);
	buildRectFace( verts[16], verts[17], verts[18], verts[19], colors.dirt);
}

function hydrant()
{
    genSphereVertices(1, 10, 10, 0, 0, 1.5);
    plotSphere(10, 10, numVertices-121);
}

function genSphereVertices(radius, latBands, lonBands, X, Y, Z)
{
	var normRadius = 1;
	
    for (var lat = 0; lat < latBands + 1; ++lat)
	{
		var theta = lat * Math.PI/latBands; //0 to pi
		var z = normRadius * Math.cos(theta);
		
		for (var lon = 0; lon < lonBands + 1; ++lon)
		{
		    var phi = 2 * lon * Math.PI/lonBands;//0 to 2pi
			
			var u = Math.sqrt((normRadius * normRadius) - (z * z));
			
			var x = u * Math.cos(phi);
			var y = u * Math.sin(phi);
			
			pointsArray.push(vec4((X+x)*radius, (Y+y)*radius, (Z+z)*radius, 1.0));
			numVertices++;
			colorsArray.push(faceColors[lon%7]);
		}
	}
}

function plotSphere(latBands, lonBands, offset)
{
    for (var lat = 0; lat < latBands; ++lat)
	{
	    for (var lon = 0; lon < lonBands; ++lon)
		{
		    var vA = lat * (lonBands + 1) + lon + offset;
			var vB = vA + (latBands + 1);
			var vC = vA + 1;
			var vD = vB + 1;
			
			indexBuffer.push(vA);
			indexBuffer.push(vB);
			indexBuffer.push(vC);
			indexBuffer.push(vB);
			indexBuffer.push(vD);
			indexBuffer.push(vC);
		}
	}
}


function tree()
{
    var trunk = [
	    vec4(2.1, 0.55, 1.83, 1.0),
		vec4(2.1, 0.0,  1.83, 1.0),
		vec4(2.3, 0.0,  1.83, 1.0),
		vec4(2.3, 0.55, 1.83, 1.0),
		vec4(2.1, 0.55, 1.63, 1.0),
		vec4(2.1, 0.0,  1.63, 1.0),
		vec4(2.3, 0.0,  1.63, 1.0),
		vec4(2.3, 0.55, 1.63, 1.0)
	];
	
	 var leaves = [
	    vec4(1.75, 1.1,  2.3, 1.0),
		vec4(1.75, 0.55, 2.3, 1.0),
		vec4(2.65, 0.55, 2.3, 1.0),
		vec4(2.65, 1.1,  2.3, 1.0),
		vec4(1.75, 1.1,  1.33, 1.0),
		vec4(1.75, 0.55, 1.33, 1.0),
		vec4(2.65, 0.55, 1.33, 1.0),
		vec4(2.65, 1.1,  1.33, 1.0)
	];
	
	buildRectFace(trunk[0], trunk[1], trunk[2], trunk[3], colors.dirt);
	buildRectFace(trunk[3], trunk[2], trunk[6], trunk[7], colors.dirt);
	buildRectFace(trunk[7], trunk[6], trunk[5], trunk[4], colors.dirt);
	buildRectFace(trunk[4], trunk[5], trunk[1], trunk[0], colors.dirt);
	
	buildRectFace(leaves[0], leaves[1], leaves[2], leaves[3], colors.leaves);
	buildRectFace(leaves[3], leaves[2], leaves[6], leaves[7], colors.leaves);
	buildRectFace(leaves[7], leaves[6], leaves[5], leaves[4], colors.leaves);
	buildRectFace(leaves[4], leaves[5], leaves[1], leaves[0], colors.leaves);
	buildRectFace(leaves[4], leaves[0], leaves[3], leaves[6], colors.leaves);
	buildRectFace(leaves[5], leaves[1], leaves[2], leaves[7], colors.leaves);
	
}