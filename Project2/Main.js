var canvas;
var gl;
var program;
var context;
var rect;

var objectDataStr = '';

var curveIndex = 0;
var handleIndex = 0;

class Handle{
    constructor(x, y, offset)
	{
	    this.x = x;
		this.y = y;
		this.offset = offset;
	}
}

var allHandles = [
    // [
       // [-122,  19],
	   // [ -79, -99],
	   // [  55, -90],
       // [ 135, -15]  ],
	// [
       // [-122,  19],
	   // [   4, 134],
	   // [ 129,  46],
       // [ 135, -15]  ],
	// [
       // [ -53, -59],
	   // [ -81, -16],
	   // [ -68,  48],
       // [ -17,  73]  ],
	[
       [  53, -64],
	   [ 102, -50],
	   [ 119,  32],
       [  70,  62]  ],
	// [
       // [  13, -21],
	   // [  60, -15],
	   // [  51,  34],
       // [  12,  35]  ],
	// [
       // [  13, -21],
	   // [ -30, -15],
	   // [ -21,  34],
       // [  12,  35]  ]
];

var bCurveObjects = [];

var mvMatrix, pMatrix;
var mvMatrixLoc, pMatrixLoc;

var pointsArray = [];
var colorsArray = [];

var eyeVec = vec3( 0, 0, 530);
var atVec = vec3( 0, 0, 0 );
var upVec = vec3( 0, 1, 0 );

window.onload = function()
{
    canvas = document.getElementById( "canvas" );
	
	gl = WebGLUtils.setupWebGL( canvas );
	if ( !gl ) { alert( "WebGL isn't available" ); }
	
	//vertex/fragment shaders
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
	
	initCameraMatrices();
	
	drawEye();
	
	document.body.addEventListener( "mousedown", onMouseDown );
	
	objectDataStr = dataToString();
	
	loadBuffers();
	render();
}

function initCameraMatrices()
{
    mvMatrix = lookAt( eyeVec, atVec, upVec );
	
	fovy = 50;
	aspect = canvas.width/canvas.height;
	near = 1;
	far = 1000;
	
	pMatrix = perspective( fovy, aspect, near, far );
	
	gl.viewport( 0, 0, canvas.width, canvas.height );
	gl.clearColor( 0.9, 0.9, 1.0, 1.0 );
}

function onMouseDown()
{
    rect = canvas.getBoundingClientRect();
			
	var x = (event.clientX - rect.left) / (rect.right - rect.left) * canvas.width;
	var y = (event.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height;
	
	x = x >= canvas.width/2 ? (x - canvas.width/2) : -(canvas.width/2 - x);
	y = y >= canvas.hieght/2 ? (y - canvas.height/2) : (canvas.height/2 - y);
	
	//render each of curves based on handle locations
	var found = false;
	for (var i = 0; i < bCurveObjects.length; i++)
	{
		curveIndex = i;
		var currHandles = bCurveObjects[curveIndex][0];
		
		for (var j = 0; j < currHandles.length; j++)
		{
			handleIndex = j;
			var h = bCurveObjects[curveIndex][0][handleIndex];
			
			//if cursor is within a handle bound grab it
			if (x >= h.x-7 && x <= h.x+7 &&  y >= h.y-7 && y <= h.y+7)
			{
				document.body.addEventListener( "mousemove", onMouseMove );
				document.body.addEventListener( "mouseup", onMouseUp );
				found = true;
				break;
			}
		}
		if (found) break;
	}
}

function onMouseMove()
{   
    //when the mouse moves update the handle location and draw
    var x = (event.clientX - rect.left) / (rect.right - rect.left) * canvas.width;
	var y = (event.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height;
			
	x = x >= canvas.width/2 ? (x - canvas.width/2) : -(canvas.width/2 - x);
	y = y >= canvas.hieght/2 ?  -(y - canvas.height/2) : (canvas.height/2 - y);
	
	//save the new handle location
	bCurveObjects[curveIndex][0][handleIndex].x = x;
	bCurveObjects[curveIndex][0][handleIndex].y = y;
	
	//calculate new sqauare verts
	square(x, y, bCurveObjects[curveIndex][0][handleIndex].offset);

	//generate a curve based on new handle
	bCurveObjects[curveIndex][1].generateCurve( bCurveObjects[curveIndex][0] );
	
	//draw
	loadBuffers();
	render();
}

function onMouseUp()
{	
	document.body.removeEventListener( "mousemove", onMouseMove );
	document.body.removeEventListener( "mouseup", onMouseUp );
}

function drawEye()
{
    //move through each point and plot handle locations then generate bezier from them
	for (var i = 0; i < allHandles.length; i++)
	{
		var curveHandles = allHandles[i];
		var newHandles = [];
		
		//for each handle
		for (var j = 0; j < curveHandles.length; j++)
		{
			var curveHandleX = curveHandles[j][0];
			var curveHandleY = curveHandles[j][1];
			
			newHandles.push( new Handle(curveHandleX, curveHandleY, pointsArray.length) );
			
			//square peg for dragging
			square( curveHandleX, curveHandleY, pointsArray.length );
		}
		
		//generate bezier from the handle locations
		var b = new BezierCurve( pointsArray.length );
		b.generateCurve( newHandles );
		
		bCurveObjects.push( [newHandles, b] );
	}
}

function square( x, y, offset )
{   
	var verts = [
		vec4( x-5, y+5, 0, 1 ),
		vec4( x-5, y-5, 0, 1 ),
		vec4( x+5, y-5, 0, 1 ),
		vec4( x+5, y+5, 0, 1 )
	];
	
	var indices = [0, 1, 2, 0, 2, 3];
	for (var i = 0; i < indices.length; i++)
	{
		pointsArray[offset+i] = verts[ indices[i] ];
		colorsArray[offset+i] = vec4( 0.9, 0.1, 0.9, 1.0);
	}
}

function dataToString()
{
	var str = "";
    for (var i = 0; i < bCurveObjects.length; i++)
	{
	    for (var j = 0; j < bCurveObjects[i][0].length; j++)
		{
		    str += '(' + bCurveObjects[i][0][j].x + ", "
			           + bCurveObjects[i][0][j].y + ')';
		}
		str += "\r\n";
	}
	return str;
}

function loadBuffers()
{
    //vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten( pointsArray ), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	//colors
	var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten( colorsArray ), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
	//camera
    mvMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    pMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
	
	gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );
	gl.uniformMatrix4fv( pMatrixLoc, false, flatten(pMatrix) );
}

function renderHandles()
{	
	for (var i = 0; i < bCurveObjects.length; i++)
	{
		for (var j = 0; j < bCurveObjects[i][0].length; j++)
		{
		    gl.drawArrays( gl.TRIANGLES, bCurveObjects[i][0][j].offset, 6 );
		}
	}
}

function renderBezier()
{
	for (var i = 0; i < bCurveObjects.length; i++)
	{
	    gl.drawArrays( gl.LINE_STRIP, bCurveObjects[i][1].offset, 100);
	}
}

function render()
{
	gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
	
    renderHandles();
	renderBezier();
}