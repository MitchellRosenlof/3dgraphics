var textRead = "";

function saveToFile() {


    var textBlob = new Blob([objectDataStr], {type:"text/plain"});
    
    var textToSaveAsURL = window.URL.createObjectURL(textBlob);
    
    var saveFileName = document.getElementById("saveFileName").value;
 
    var downloadLink       = document.createElement("a");
    downloadLink.download  = saveFileName;
    
    downloadLink.innerHTML = "Download File";
    downloadLink.href      = textToSaveAsURL;
    downloadLink.onclick   = destroyClickedElement;
    
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
 
    downloadLink.click();
}

 
function destroyClickedElement(event) {
    document.body.removeChild(event.target);
    
}

 
function loadFromFile() {

    var fileToLoad = document.getElementById("fileToLoad").files[0];
 
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent) {
    
        var textFromFileLoaded = fileLoadedEvent.target.result;
        
        textRead = textFromFileLoaded;
        
        console.log(textRead);

    };
    
    fileReader.readAsText(fileToLoad, "UTF-8");
}
 

