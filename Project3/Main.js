"use strict";

var bLUT = [    [1],
               [1, 1],
			  [1, 2, 1],
             [1, 3, 3, 1],
            [1, 4, 6, 4, 1],
          [1, 5, 10, 10, 5, 1],
        [1, 6, 15, 20, 15, 6, 1]
];

// draws wire frame teapot data
// by evaluating Bezier polynomials
// for each patch
var controlPoints = [
    vec2( -150, 0 ),
	vec2( -100, 100 ),
	vec2( 100, 100 ),
	vec2( 150, 0 )
];
var canvas, gl, program;
// number of evaluations along each curve

var footballRadius = 50;
var theta = 0;
var phi = 0;
var radius = 500;

var pointsArray = [];
var colorsArray = [];
var indexBuffer = [];

var eye = vec3(0, 0, 500);
var at = vec3(0, 0, 0);
var up = vec3(0, 1, 0);

var mvMatrix, pMatrix, mvMatrixLoc, pMatrixLoc;


onload = function init()  {

    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
  
	program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
	
	initCameraMatrices();
	
	football( 20 );
	plotFootball( 20 );
	
	loadBuffers();
	
	console.log(pointsArray);
	
	document.getElementById("Button1").onclick = function(){ 
	    theta += .1;
		render();
	};
    document.getElementById("Button2").onclick = function(){
        theta -= .1; 
		render();
	};
	
	document.getElementById("Button3").onclick = function(){ 
	    phi += .1;
		render();
	};
    document.getElementById("Button4").onclick = function(){
        phi -= .1; 
		render();
	};	

    render();
}

function football(numBands)
{	
    for (var i = 0; i < numBands + 1; i++)
	{
		var theta = 2 * i * Math.PI/numBands; //0 to pi
		var z = Math.sin(theta);
		
		generateCurve( controlPoints, z, theta );
	}
}

function plotFootball(numBands)
{
    for (var i = 0; i < numBands; i++)
	{
	    for (var j = 0; j < 100; j++)
		{
		    var vA = i * (numBands + 1) + j;
			var vB = vA + (numBands + 1);
			var vC = vA + 1;
			var vD = vB + 1;
			
			indexBuffer.push(vA);
			indexBuffer.push(vB);
			indexBuffer.push(vC);
			indexBuffer.push(vB);
			indexBuffer.push(vD);
			indexBuffer.push(vC);
		}
	}
}
	
function binomial( n, k )
{
	while ( n >= bLUT.length )
	{
		var prev = bLUT.length - 1;
		var nextRow = [];
		
		nextRow.push(1);
		
		for (var i = 1; i <= prev; i++)
		{
			nextRow.push( bLUT[prev][i-1] + bLUT[prev][i] );
		}
		
		nextRow.push(1);
		bLUT.push( nextRow );
	}
	return bLUT[n][k];
}

function Bezier( t, w )
{
	var n = w.length - 1;
	var sum = 0;
	
	for (var k = 0; k < w.length; k++)
	{
		sum += w[k] * binomial(n, k) * Math.pow(1-t, n-k) * Math.pow(t, k);
	}
	return sum;
}

function generateCurve( points, z, theta )
{
	var xControl = [];
	var yControl = [];
	for (var i = 0; i < points.length; i++)
	{
	   xControl.push( points[i][0] );
	   yControl.push( points[i][1] );
	}

	var i = 0;
	for (var t = 0; t < 1.0; t += .01)
	{		
        var phi = i * Math.PI/100;//0 to 2pi
		var u = Math.sqrt(1-(z * z));
		
		var x = Math.cos(theta) * Bezier( t, xControl );
		
		var y = Math.cos(theta) * Bezier( t, yControl );
		
		pointsArray.push( vec4( x, y, z*50, 1 ) );
		colorsArray.push( vec4() );
		i++;
	}
}

function initCameraMatrices()
{
    mvMatrix = lookAt( eye, at, up );
	
	var fovy = 50;
	var aspect = canvas.width/canvas.height;
	var near = 1;
	var far = 1000;
	
	pMatrix = perspective( fovy, aspect, near, far );
	
	gl.viewport( 0, 0, canvas.width, canvas.height );
	gl.clearColor( 0.9, 0.9, 1.0, 1.0 );
}

function updateCamera()
{
    //updates camera to new view
	eye = vec3(radius*Math.sin(theta)*Math.cos(phi),
               radius*Math.sin(theta)*Math.sin(phi),
			   radius*Math.cos(theta));
    mvMatrix = lookAt(eye, at, up);
	gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );
}


function loadBuffers()
{
	var iBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexBuffer), gl.STATIC_DRAW);
	iBuffer.itemSize = 1;
	iBuffer.numItems = indexBuffer.length;
	
    //vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten( pointsArray ), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	//colors
	// var cBuffer = gl.createBuffer();
    // gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    // gl.bufferData( gl.ARRAY_BUFFER, flatten( colorsArray ), gl.STATIC_DRAW );

    // var vColor = gl.getAttribLocation( program, "vColor" );
    // gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    // gl.enableVertexAttribArray( vColor );
	
	//camera
    mvMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    pMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
	
	gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );
	gl.uniformMatrix4fv( pMatrixLoc, false, flatten(pMatrix) );
}

var render = function(){
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	updateCamera();
	for (var i = 0; i < 20; i++)
	   gl.drawArrays( gl.LINE_STRIP, i * 100, 100 );
    //gl.drawElements(gl.LINE_STRIP, indexBuffer.length, gl.UNSIGNED_SHORT, 0);
    //requestAnimFrame(render);
}
