"use strict";

// draws wire frame teapot data
// by evaluating Bezier polynomials
// for each patch

var canvas, gl, program;
// number of evaluations along each curve

var numDivisions = 5;

var index = 0;
var theta = 0;
var phi = 0;
var radius = 10;

var points = [];

var bezier = function(u) {
    var b = [];
    var a = 1-u;
	
    b.push(u*u*u);
    b.push(3*a*u*u);
    b.push(3*a*a*u);
    b.push(a*a*a);

    return b;
}

var eye = vec3(0, 5, 5);
var at = vec3(0, 0, 0);
var up = vec3(0, 1, 0);

var mvMatrix, pMatrix, mvMatrixLoc, pMatrixLoc;



onload = function init()  {

    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    var h = 1.0/numDivisions;

    var patch = new Array(numTeapotPatches);  // allocates an array of 32 patches
    
    for(var i=0; i<numTeapotPatches; i++) 
    	patch[i] = new Array(16);            // each array is an array of 16 elements
    	
    //	
    //  The following essentially converts the array of vertex points and indices
    //  to just a patch array(4x4) of ordered control points (u x v)
    //  note the vertices are stored x, z, y so had to flip y and z values
    //
    
    for(var i=0; i<numTeapotPatches; i++)
        for(j=0; j<16; j++) {
            patch[i][j] = vec4([vertices[indices[i][j]][0],
                                vertices[indices[i][j]][2],
                                vertices[indices[i][j]][1], 1.0]);
        }


    for ( var n = 0; n < numTeapotPatches; n++ ) {

		var data = new Array(numDivisions+1);
	
		for(var j = 0; j<= numDivisions; j++) 
		  data[j] = new Array(numDivisions+1);
	    
		for(var i=0; i<=numDivisions; i++) 
		  for(var j=0; j<= numDivisions; j++) {
	  
			data[i][j] = vec4(0,0,0,1);
		
			var u = i*h;   // u = 0..1 (divisions of 1/10)
			var v = j*h;   // v = 0..1 (divisions of 1/10)
		
			var t = new Array(4);
		
			for(var ii=0; ii<4; ii++) 
			  t[ii]=new Array(4);
		
			for(var ii=0; ii<4; ii++) 
			  for(var jj=0; jj<4; jj++) { 
			    
				t[ii][jj] = bezier(u)[ii]*bezier(v)[jj];
				
			  };	

			for(var ii=0; ii<4; ii++) 
			  for(var jj=0; jj<4; jj++) {
			  
			    // For each vertice in the patch
			    // scale it by the bezier (u * v) factor
		  
				var temp = vec4(patch[n][4*ii+jj]);
			
				temp = scale( t[ii][jj], temp);  // Bezier scalar * weight
			
				data[i][j] = add(data[i][j], temp);  // temp is added to the data

			}
		}


		// stores points to be drawn in orders of 3 points per tri 6 per quad patch
	
		for(var i=0; i<numDivisions; i++) for(var j =0; j<numDivisions; j++) {
	
		   /*  A   B
			   C   D
		   
			   A, C , D, A, C, B
		   
			*/
	
			points.push(data[i][j]);              // Pt A
			points.push(data[i+1][j]);            // Pt A + num divisions
			points.push(data[i+1][j+1]);		  // Pt A + num divisions + 1
			points.push(data[i][j]);              // Pt A 
			points.push(data[i+1][j+1]);		  // Pt A + num divisions + 1
			points.push(data[i][j+1]);            // Pt A + 1
			index += 6;
		}
    }
	
	program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
	
	
	initCameraMatrices();
	loadBuffers();
	
	console.log(points);
	
	document.getElementById("Button1").onclick = function(){ 
	    theta += .1;
		render();
	};
    document.getElementById("Button2").onclick = function(){
        theta -= .1; 
		render();
	};
	
	document.getElementById("Button3").onclick = function(){ 
	    phi += .1;
		render();
	};
    document.getElementById("Button4").onclick = function(){
        phi -= .1; 
		render();
	};	

    render();
}

function initCameraMatrices()
{
    mvMatrix = lookAt( eye, at, up );
	
	var fovy = 50;
	var aspect = canvas.width/canvas.height;
	var near = 1;
	var far = 100;
	
	pMatrix = perspective( fovy, aspect, near, far );
	
	gl.viewport( 0, 0, canvas.width, canvas.height );
	gl.clearColor( 0.9, 0.9, 1.0, 1.0 );
}

function updateCamera()
{
    //updates camera to new view
	eye = vec3(radius*Math.sin(theta)*Math.cos(phi),
               radius*Math.sin(theta)*Math.sin(phi),
			   radius*Math.cos(theta));
    mvMatrix = lookAt(eye, at, up);
}


function loadBuffers()
{
    //vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten( points ), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	//colors
	// var cBuffer = gl.createBuffer();
    // gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    // gl.bufferData( gl.ARRAY_BUFFER, flatten( colorsArray ), gl.STATIC_DRAW );

    // var vColor = gl.getAttribLocation( program, "vColor" );
    // gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    // gl.enableVertexAttribArray( vColor );
	
	//camera
    mvMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    pMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
	
	gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );
	gl.uniformMatrix4fv( pMatrixLoc, false, flatten(pMatrix) );
}

var render = function(){
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	loadBuffers();
	updateCamera();

    for(var i=0; i<index; i+=3) gl.drawArrays( gl.LINE_LOOP, i, 3 );
    //requestAnimFrame(render);
}
