var bLUT = [    [1],
               [1, 1],
			  [1, 2, 1],
             [1, 3, 3, 1],
            [1, 4, 6, 4, 1],
          [1, 5, 10, 10, 5, 1],
        [1, 6, 15, 20, 15, 6, 1]
];

	
binomial( n, k )
{
	while ( n >= bLUT.length )
	{
		var prev = bLUT.length - 1;
		var nextRow = [];
		
		nextRow.push(1);
		
		for (var i = 1; i <= prev; i++)
		{
			nextRow.push( bLUT[prev][i-1] + bLUT[prev][i] );
		}
		
		nextRow.push(1);
		bLUT.push( nextRow );
	}
	return bLUT[n][k];
}

Bezier( t, w )
{
	var n = w.length - 1;
	var sum = 0;
	
	for (var k = 0; k < w.length; k++)
	{
		sum += w[k] * binomial(n, k) * Math.pow(1-t, n-k) * Math.pow(t, k);
	}
	return sum;
}

generateCurve( points, z )
{
	var xControl = [];
	var yControl = [];
	for (var i = 0; i < controlPoints.length; i++)
	{
	   xControl.push( points[i].x );
	   yControl.push( points[i].y );
	}

	
	for (var t = 0; t < 1.0; t += .01)
	{
		var x = Bezier( t, xControl );
		var y = Bezier( t, yControl );
		
		pointsArray.push( vec4( x, y, z, 1 ));
		colorsArray.push( vec4() );
	}
}