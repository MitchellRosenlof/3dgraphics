var results = [];//results of each tree
class Lsys{
	//object to hold lsys data
    constructor(id, n, angle, seed, rules, string)
	{
	    this.id = id;
		this.n = n;
		this.angle = angle;
		this.seed = seed;
		this.rules = rules;
		this.string = string;
	}
}


function generate_tree_grammar(seed, rules, n)
{
	//take in a seed and recursively grow it based on grammar rules
	var result = '';
	if (n > 0)
	{
		for (var i = 0; i < seed.length; ++i)
	    {
			//swap based on rules
		    if (seed[i] == 'F')
			{
				result += rules[seed[i]];
			}else{
				result += seed[i];
			}
	    }
		//next iteration
		seed = result;
		generate_tree_grammar(seed, rules, n - 1);
	}else{
		//push result
		results.push(seed);
	}
}

function create_lsystems()
{
    var t1 = {'F':'F[+F]F[-F]F'};
	var t2 = {'F':'FF-[-F+F+F]+[+F-F-F]'};
	var t3 = {'F':'F[&+F]F[-/F][-/F][&F]'};
	var t4 = {'F':'F[-&\F][\++&F]||F[--&/F][+&F]'};

	var rules = [t1, t2, t3, t4];
	
	//generate lsys strings
	generate_tree_grammar('F', t1, 3);
	generate_tree_grammar('F', t2, 3);
	generate_tree_grammar('F', t3, 3);
	generate_tree_grammar('F', t4, 3);
	
	//store lsys data
	var lsys1 = new Lsys(1, 5, 25.7, 'F', rules[0], results[0]);
	var lsys2 = new Lsys(2, 4, 22.5, 'F', rules[1], results[1]);
	var lsys3 = new Lsys(3, 3, 28.0, 'F', rules[2], results[2]);
	var lsys4 = new Lsys(4, 3, 22.5, 'F', rules[3], results[3]);
	
	lsys_data = [lsys1, lsys2, lsys3, lsys4];
}