var branch_colors = [
	[ 0.5, 0.1, 0.1, 1.0 ], //different shades of brown
	[ 0.5, 0.15, 0.1, 1.0 ],
	[ 0.5, 0.2, 0.1, 1.0 ],
	[ 0.5, 0.25, 0.1, 1.0 ],
	[ 0.5, 0.3, 0.1, 1.0 ],
	[ 0.5, 0.35, 0.1, 1.0 ],
	[ 0.5, 0.4, 0.1, 1.0 ]
];

var leaf_colors = [
    [ 0.5, 0.9, 0.1, 1.0 ],
	[ 0.5, 0.85, 0.1, 1.0 ],
	[ 0.5, 0.8, 0.1, 1.0 ],
	[ 0.5, 0.75, 0.1, 1.0 ],
	[ 0.5, 0.7, 0.1, 1.0 ],
	[ 0.5, 0.65, 0.1, 1.0 ],
	[ 0.5, 0.6, 0.1, 1.0 ]
];

class Cylinder{
    constructor(h, r){
		this.cyl_verts = [];
		this.num_cly_indices = 0;
		this.num_cly_verts = 0;
		this.instanceMatrix = mat4();
		
		//used to link for polycylinder
		this.base_loc = vec3(0, 0, 0);
		this.top_loc = vec3(0, h, 0);
		
		var x_base, y_base, z_base, x_top, y_top, z_top;
	    for (var i = 0; i < 2*Math.PI+.1; i+=0.1)
		{
			//find base circle vertices
		    x_base = r * Math.cos(i);
			z_base = r * Math.sin(i);
			y_base = 0;
			
			points_array.push(vec4(x_base, y_base, z_base, 1.0));
			colors_array.push(branch_colors[Math.floor(Math.random() * 6)]);
			
			//find top circle vertices
			x_top = r * Math.cos(i);
			z_top = r * Math.sin(i);
			y_top = h;
			
			points_array.push(vec4(x_top, y_top, z_top, 1.0));
			colors_array.push(branch_colors[Math.floor(Math.random() * 6)]);
			this.num_cly_verts += 2;
			total_vertices += 2;
			
			this.cyl_verts.push(vec4(x_base, y_base, z_base, 1.0));
			this.cyl_verts.push(vec4(x_top, y_top, z_top, 1.0));
		}
	}
	
	plot_cylinder(num_verts, offset)
	{
		for (var i = offset; i < num_verts+offset-2; i+=2)
		{
			//four indices of mesh "block"
			var vA = i;
			var vB = i+1;
			var vC = i+2;
			var vD = i+3;
			
			//two triangles of square face used to draw 
			index_buffer.push(vA);
			index_buffer.push(vB);
			index_buffer.push(vD);
			index_buffer.push(vA);
			index_buffer.push(vC);
		    index_buffer.push(vD);
			
			this.num_cly_indices+=6;
		}
	}
	
	get_num_verts(){ return this.num_cly_verts; }
	
	get_num_indices(){ return this.num_cly_indices; }
	
	get_links(){ return [this.base_loc, this.top_loc] }
}

class Sphere{
    constructor(radius, latBands, lonBands)
    {	
	    this.sphere_verts = 0;
		this.sphere_indices = 0;
		
		//rotate arround latitude and longitude and plot circles
        for (var lat = 0; lat < latBands + 1; ++lat)
	    {
			var theta = lat * Math.PI/latBands; //0 to pi
			var z = radius * Math.cos(theta); //distance from center
			
		    for (var lon = 0; lon < lonBands + 1; ++lon)
			{
				var phi = 2 * lon * Math.PI/lonBands;//0 to 2pi
				
				//circle points at current rotation
				var u = Math.sqrt((radius * radius) - (z * z));

			    var x = u * Math.cos(phi);
			    var y = u * Math.sin(phi);
			
			    points_array.push(vec4(x, y, z, 1.0));
			    colors_array.push(leaf_colors[Math.floor(Math.random() * 6)]);
				
				this.sphere_verts++;
				total_vertices++;
			}
	    }
    }

	plot_sphere(latBands, lonBands, offset)
	{
		for (var lat = 0; lat < latBands; ++lat)
		{
			for (var lon = 0; lon < lonBands; ++lon)
			{
				//find four vertices of mesh "block"
				var vA = lat * (lonBands + 1) + lon + offset;
				var vB = vA + (latBands + 1);
				var vC = vA + 1;
				var vD = vB + 1;
				
				//push the indices of the two traingles that make up the face
				index_buffer.push(vA);
				index_buffer.push(vB);
				index_buffer.push(vC);
				index_buffer.push(vB);
				index_buffer.push(vD);
				index_buffer.push(vC);
				
				this.sphere_indices += 6;
			}
		}
	}
	
	get_num_verts(){ return this.sphere_verts; }
	
	get_num_indices(){ return this.sphere_indices; }
}