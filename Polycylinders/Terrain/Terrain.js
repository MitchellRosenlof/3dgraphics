var canvas;
var gl;
var program;

var branch_len = .019;
var num_each_type = 30;

var mount_smooth_factor = 3;

var lsys_data = [];
var stack = [];

var cylinder;
var sphere;
var seed_locations = [];
var planted_indices = [];
var total_vertices = 0;

var points_array = [];
var colors_array = [];

var index_buffer = [];
var i_buffer;

var terr_verts = [];
var terr_indices = 0;

var x_terr = 70;
var z_terr = 70;

var terrain_colors = [
	[ 1.0, 1.0, 1.0, 1.0 ],  //snowy white
	[ 0.9, 0.9, 0.9, 1.0 ],  //darker snow
	[ 0.8, 0.8, 0.8, 1.0 ],  //darker snow
	[ 0.5, 0.5, 0.5, 1.0 ],  //rocky grey
	[ 0.4, 0.3, 0.1, 1.0 ],  //brown
	[ 0.1, 0.6, 0.1, 1.0 ],  //v different shades of green v
	[ 0.1, 0.55, 0.1, 1.0 ],
	[ 0.1, 0.50, 0.1, 1.0 ],
	[ 0.1, 0.45, 0.1, 1.0 ],
	[ 0.1, 0.4, 0.1, 1.0 ]
];
var radius = 4.0;
var theta  = 2.5;
var phi    = 2.5;
var dr = 5.0 * Math.PI/180.0;

var mvMatrix, pMatrix;
var mvMatrixLoc, pMatrixLoc;

var eye;
const at = vec3(0.0, 0.5, 0.0);
const up = vec3(0.0, 1.0, 0.0);

window.onload = function init()
{
	//create a canvas
    canvas = document.getElementById( "gl-canvas" );

	//ensure no errors
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
	
	//create 3D effect
	gl.enable(gl.DEPTH_TEST);
	
	//vertex/fragment shaders
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
	
	//init camera
	update_camera();
	
	fovy = 50;
	aspect = canvas.width/canvas.height;
	near = .001;
	far = 5;
	
	pMatrix = perspective(fovy, aspect, near, far);
	
	//create mountain and smooth out peaks
	generate_terrain();
	for (var i = 0; i < mount_smooth_factor; ++i){ round_mountains(); }
	push_colors();
	plot_terrain();
	
	//plot trees at random location
	terr_verts = points_array.slice();
	
	//create cly and sphere for matrices to use
	cylinder = new Cylinder(branch_len, 0.005);
	cylinder.plot_cylinder( cylinder.get_num_verts(), total_vertices - cylinder.get_num_verts());
	
	sphere = new Sphere(0.004, 5, 5);
	sphere.plot_sphere(5, 5, total_vertices - sphere.get_num_verts());
	
	//buffers for shaders
    load_buffers();
	gl.uniformMatrix4fv( pMatrixLoc, false, flatten(pMatrix) );
	
	//view the canvas
    gl.viewport( 0, 0, canvas.width, canvas.height );
	
	//generate grammar of fractal trees (for project 7)
	create_lsystems();
	plant_seeds();

	//light blue sky
    gl.clearColor( 0.01, 0.5, 1.0, 0.2 );
	
	//buttons for camera rotation around terrain
	document.getElementById("Button1").onclick = function(){ 
	    theta += dr;
		update_camera();
		render(); 
	};
    document.getElementById("Button2").onclick = function(){
        theta -= dr;
		update_camera();
		render(); 
	};
    document.getElementById("Button3").onclick = function(){
        phi += dr;
		update_camera();
		render(); 
	};
    document.getElementById("Button4").onclick = function(){
	    phi -= dr;
		update_camera();
		render(); 
	};
	document.getElementById("Button5").onclick = function(){
		radius -= dr;
		update_camera();
		render(); 
	};
	
	document.getElementById("Button6").onclick = function(){
		radius += dr;
		update_camera();
		render(); 
	};

	//render the scene
    render();
}

function load_buffers()
{
	//vertices
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points_array), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	//colors
	var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors_array), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
	//indices of terrain and polycylinders meshes
	i_buffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, i_buffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(index_buffer), gl.STATIC_DRAW);

	//camera
    mvMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    pMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
}

function generate_terrain()
{
	//grid x length centered at origin (if 10 move from -5 to 5)
    for (var i = Math.floor(-x_terr/2); i < (Math.floor(x_terr/2)); i++)
	{//grid z length centered at origin
	    for (var j = Math.floor(-z_terr/2); j < (Math.floor(z_terr/2)); j++)
		{
			//scale plane to view size coords
		    var x = i/x_terr*3;
			var z = j/z_terr*3;
			
			//create a point that is close to others (also randomized) based on cosine value
			var y = 3 * Math.abs(Math.cos(z) * Math.cos(x) * Math.random());
			
			//push to array for drawing
			points_array.push(vec4(x, y, z, 1.0));
			
			total_vertices++;
		}
	}
}

function round_mountains()
{
	//function moves through points array and makes current point the average of
	//itself and surrounding points
    for (var x = 0; x < x_terr; ++x)
	{    
        for (var z = 0; z < z_terr; ++z)
		{
			//above left below right
			var N = points_array[x_terr * x + z - x_terr];
			var W = points_array[x_terr * x + z - 1];
			var S = points_array[x_terr * x + z + x_terr];
			var E = points_array[x_terr * x + z + 1];

			//if on outside edge of the "grid" elevation is 0
			if ((x == 0) || (z == 0) || (x == x_terr-1) || (z == z_terr-1)){ 
			    points_array[x_terr * x + z][1] = 0; 
			}else{
				//else avg them
			    N = N[1];
				W = W[1];
				S = S[1];
				E = E[1];
			    points_array[x_terr * x + z][1] = (points_array[x_terr * x + z][1] + N + W + S + E) / 5; 
			}
		}
	}
}

function push_colors()
{
	//pushes colors to mountain verts based on altitude
    for (var x = 0; x < x_terr; ++x)
	{    
        for (var z = 0; z < z_terr; ++z)
		{			
			//add colors based on vertex altitude
			var y = points_array[x_terr * x + z][1];
			if (y > 1){ colors_array.push(terrain_colors[Math.floor(Math.random() * 2)]); 
			}else if (y > .8){ colors_array.push(terrain_colors[3]); 
			}else if (y > .6){ colors_array.push(terrain_colors[4]);
			}else{ colors_array.push(terrain_colors[Math.floor(Math.random() * (8 - 5 + 1) + 5)]) }
		}
	}
}

function plot_terrain()
{
    for (var i = 0; i < x_terr; ++i)
	{
	    for (var j = 0; j < z_terr; ++j)
		{
			//find six indices of two triangles for each "block" of mesh to be drawn
		    var vA = i * (x_terr) + j;
			var vB = vA + (z_terr);
			var vC = vA + 1;
			var vD = vB + 1;
			
			index_buffer.push(vA);
			index_buffer.push(vB);
			index_buffer.push(vD);
			index_buffer.push(vA);
			index_buffer.push(vC);
			index_buffer.push(vD);
			
			terr_indices+=6;
		}
	}
}

function find_unique_index(i)
{
	//chooses random index within terrain verts bounds until a unique one is generated
	var new_index = i;
    while (planted_indices.includes(new_index)){
	    new_index = Math.floor(Math.random() * terr_verts.length);
	}
	return new_index;
}

function plant_seeds()
{
	//generates array of indices of each tree at non-repeating random points
    for (var i = 0; i < lsys_data.length; ++i)
	{
	    for (var j = 0; j < num_each_type; ++j)
		{
			var r = Math.floor(Math.random() * terr_verts.length);
			var newi = find_unique_index(r);
			seed_locations.push( terr_verts[newi] );
		    planted_indices.push(newi);
		}
	}
}

function pin_lsys_trees()
{
	//places trees based on unique indices generated
    for (var i = 0; i < seed_locations.length; ++i)
	{
		generate_tree(lsys_data[i%4], seed_locations[i]);
	}
}

function generate_tree(lsys, seed)
{
    //save view matrix to remember global reference
	stack.push(mvMatrix);
	
	//transformations
	var trans_mat = translate(vec3(seed));
	var rot_mat = mat4();
	var scale_mat = scalem(.9, .9, .9);
	
	//draw first trunk at location on mountain
	mvMatrix = mult( mvMatrix, trans_mat );
	renderPC();	
	
	//move through lsystem string and draw polycyls based on transformed view matrix
	var turtle_str = lsys.string;
	for (var i = 0; i < turtle_str.length; ++i)
	{
	    if (turtle_str[i] == 'F'){
			mvMatrix = mult(mvMatrix, scale_mat);
		    renderPC();
		}else if (turtle_str[i] == '-'){
		    rot_mat = rotate(-lsys.angle, [1, 0, 0]);
			mvMatrix = mult(mvMatrix, rot_mat);
		}else if (turtle_str[i] == '+'){
		    rot_mat = rotate(lsys.angle, [1, 0, 0]);
			mvMatrix = mult(mvMatrix, rot_mat);
		}else if (turtle_str[i] == '&'){
		    rot_mat = rotate(lsys.angle, [0, 1, 0]);
			mvMatrix = mult(mvMatrix, rot_mat);
		}else if (turtle_str[i] == '\\'){
		    rot_mat = rotate(lsys.angle, [0, 0, 1]);
			mvMatrix = mult(mvMatrix, rot_mat);
		}else if (turtle_str[i] == '/'){
			rot_mat = rotate(-lsys.angle, [0, 0, 1]);
			mvMatrix = mult(mvMatrix, rot_mat);
		}else if (turtle_str[i] == '|'){
		    rot_mat = rotate(180, [1, 0, 0]);
			mvMatrix = mult(mvMatrix, rot_mat);
		}else if (turtle_str[i] == '['){
		    stack.push(mvMatrix);
		}else if (turtle_str[i] == ']'){
		    mvMatrix = stack.pop();
		}
	}
	//reset view matrix to global view
	mvMatrix = stack.pop();
}

function update_camera()
{
    //updates camera to new view
	eye = vec3(radius*Math.sin(theta)*Math.cos(phi),
               radius*Math.sin(theta)*Math.sin(phi),
			   radius*Math.cos(theta));
    mvMatrix = lookAt(eye, at, up);
}

function renderPC(){
	//draw clyinder with respect to current view matrix
    gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );
	
	gl.drawElements(gl.TRIANGLES, cylinder.get_num_indices(), 
	                gl.UNSIGNED_SHORT, 2 * terr_indices);
	
	//draw a sphere at the top of the cylinder
	mvMatrix = mult(mvMatrix, translate(0.0, branch_len, 0.0));
	
	gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );
					
    gl.drawElements(gl.TRIANGLES, sphere.get_num_indices(), 
	                gl.UNSIGNED_SHORT, 2 * (terr_indices + cylinder.get_num_indices()) );						
}

function render(){
	//draw the sky
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	//translate matrices to shader code
    gl.uniformMatrix4fv( mvMatrixLoc, false, flatten(mvMatrix) );

    // render mountain
	for (var i = 0; i < x_terr; i++){
        gl.drawElements( gl.TRIANGLES, terr_indices/x_terr, gl.UNSIGNED_SHORT, 2 * i * terr_indices/x_terr );
	}
	pin_lsys_trees();
}